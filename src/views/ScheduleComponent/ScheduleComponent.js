import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import MainSection from "../HomePage/Sections/MainSection.js";
import NavPills from "components/NavPills/NavPills.js";

import baseHelper from '../../helpers/BaseHelper';

import { isEmpty } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import scheduleAction from '../../redux/futures/schedule/actions';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/rankingComponent.js";

import image from "assets/img/bg7.png";

import appConfig from '../../config/app';

const useStyles = makeStyles(styles);

export default function ScheduleComponent(props) {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const scheduleReducer = useSelector((state) => state.schedule);
    const dispatch = useDispatch();

    const classes = useStyles();
    const { ...rest } = props;

    const [streams, setStreams] = useState([]);
    const [offset, setOffset] = useState(0);
    const [key, setKey] = useState(0);

    useEffect(() => {
        dispatch(scheduleAction.getAllStreams());
    }, []);

    useEffect(() => {
        setStreams(scheduleReducer.allStreams || []);
    }, [scheduleReducer.allStreams]);

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    const onTabChange = (index) => {
        setOffset(index);
    }

    const setNewKey = () => {
        setKey(key * 2 + 1);
    }

    return (
        <div>
            <Header
                fixed
                color="transparent"
                brand="Vie bóng đá"
                rightLinks={<HeaderLinks action={() => { setNewKey() }} />}
                changeColorOnScroll={{
                    height: 80,
                    color: "white",
                }}
                {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <Card className={classes[cardAnimaton]} style={{ background: '#EEEEEE' }}>
                        <CardHeader color="primary" className={classes.cardHeader}>
                            <h1>Lịch thi đấu</h1>
                        </CardHeader>
                        <CardBody>
                            <NavPills
                                key={key}
                                alignCenter={true}
                                style={{ justifyContent: 'center' }}
                                color="rose"
                                onTabChange={onTabChange}
                                tabs={[
                                    {
                                        tabButton: "Hôm nay",
                                        tabContent:
                                            offset === 0 && streams
                                                ?
                                                <MainSection data={streams.today || []} />
                                                :
                                                null
                                    },
                                    {
                                        tabButton: "Ngày mai",
                                        tabContent:
                                            offset === 1 && streams
                                                ?
                                                <MainSection data={streams.tomorrow || []} />
                                                :
                                                null
                                    },
                                    {
                                        tabButton: "Ngày kia",
                                        tabContent:
                                            offset === 2 && streams
                                                ?
                                                <MainSection data={streams.after || []} />
                                                :
                                                null
                                    }
                                ]}
                            />
                        </CardBody>
                    </Card>

                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
