import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import baseHelper from '../../helpers/BaseHelper';
import { isEmpty } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import teamAction from '../../redux/futures/team/actions';
import playerAction from '../../redux/futures/player/actions';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/rankingComponent.js";

import image from "assets/img/bg7.png";

import appConfig from '../../config/app';

const useStyles = makeStyles(styles);

export default function RankingComponent(props) {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");

    const [teams, setTeams] = useState([]);
    const [players, setPlayers] = useState([]);

    const dispatch = useDispatch();

    const classes = useStyles();
    const { ...rest } = props;

    const teamReducer = useSelector((state) => state.team);
    const playerReducer = useSelector((state) => state.player);

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    useEffect(() => {
        dispatch(teamAction.getTeams(appConfig.TEAM.LEAGUE.PREMIER_LEAGUE));
        dispatch(playerAction.getPlayers(appConfig.TEAM.LEAGUE.PREMIER_LEAGUE));
    }, []);

    useEffect(() => {
        setTeams(teamReducer.teams || []);
    }, [teamReducer.teams]);

    useEffect(() => {
        setPlayers(playerReducer.players || []);
    }, [playerReducer.players]);

    const onRankingTabChange = (index) => {
        let league;

        switch (index) {
            case 0:
                league = appConfig.TEAM.LEAGUE.PREMIER_LEAGUE;
                break;
            case 1:
                league = appConfig.TEAM.LEAGUE.LA_LIGA;
                break;
            case 2:
                league = appConfig.TEAM.LEAGUE.SERIE_A;
                break;
            case 3:
                league = appConfig.TEAM.LEAGUE.BUNDESLIGA;
                break;
            case 4:
                league = appConfig.TEAM.LEAGUE.LIGUE_1;
                break;
            case 5:
                league = appConfig.TEAM.LEAGUE.V_LEAGUE;
                break;
        }

        dispatch(teamAction.getTeams(league));
    }

    const onRankingPlayerTabChange = (index) => {
        let league;

        switch (index) {
            case 0:
                league = appConfig.TEAM.LEAGUE.PREMIER_LEAGUE;
                break;
            case 1:
                league = appConfig.TEAM.LEAGUE.LA_LIGA;
                break;
            case 2:
                league = appConfig.TEAM.LEAGUE.SERIE_A;
                break;
            case 3:
                league = appConfig.TEAM.LEAGUE.BUNDESLIGA;
                break;
            case 4:
                league = appConfig.TEAM.LEAGUE.LIGUE_1;
                break;
            case 5:
                league = appConfig.TEAM.LEAGUE.V_LEAGUE;
                break;
            case 6:
                league = appConfig.TEAM.LEAGUE.C1;
                break;
            case 7:
                league = appConfig.TEAM.LEAGUE.C2;
                break;
        }

        dispatch(playerAction.getPlayers(league));
    }

    const renderRankingTable = () => {
        return (
            <div className="table-responsive">
                <table className="table table-shopping">
                    <thead>
                        <tr>
                            <th className="text-left col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    #
                              </div>
                            </th>
                            <th className="text-left col-md-5">
                                <div className={classes.rankingTableHeader}>
                                    CLB
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    ST
                              </div>
                            </th>
                            {/* <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    T
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    H
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    B
                              </div>
                            </th> */}
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    BT
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    BB
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    HS
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    P
                              </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {teams.map((team, index) => (
                            <tr key={index} className={index % 2 === 0 ? classes.highlighRow : ''}>
                                <td className="text-left col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {index + 1}
                                    </div>
                                </td>
                                <td className="text-left col-md-5">
                                    <div className={classes.rankingTableRow}>
                                        <div className={classes.teamNameBox}>
                                            <div className={classes.logoBox}>
                                                <img
                                                    className={classes.rankingLogo}
                                                    src={baseHelper.getTeamImageFromName(team.name)}
                                                    alt="..."
                                                />
                                            </div>
                                            <div className={classes.rankingName}>
                                                {team.name}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.game}
                                    </div>
                                </td>
                                {/* <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.win}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.draw}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.lose}
                                    </div>
                                </td> */}
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.bt}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.bb}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.hs}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {team.point}
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }

    const renderRankingPlayer = () => {
        return (
            <div className="table-responsive">
                <table className="table table-shopping">
                    <thead>
                        <tr>
                            <th className="text-left col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    #
                              </div>
                            </th>
                            <th className="text-left col-md-4">
                                <div className={classes.rankingTableHeader}>
                                    Cầu thủ
                              </div>
                            </th>
                            <th className="text-lef col-md-4">
                                <div className={classes.rankingTableHeader}>
                                    Đội bóng
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    BT
                              </div>
                            </th>
                            <th className="text-center col-md-1">
                                <div className={classes.rankingTableHeader}>
                                    PEN
                              </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {players.map((player, index) => (
                            <tr key={index} className={index % 2 === 0 ? classes.highlighRow : ''}>
                                <td className="text-left col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {index + 1}
                                    </div>
                                </td>
                                <td className="text-left">
                                    <div className={classes.rankingTableRow}>
                                        {player.name}
                                    </div>
                                </td>
                                <td className="text-left col-md-4">
                                    <div className={classes.rankingTableRow}>
                                        <div className={classes.teamNameBox}>
                                            <div className={classes.logoBox}>
                                                <img
                                                    className={classes.rankingLogo}
                                                    src={baseHelper.getTeamImageFromName(player.team)}
                                                    alt="..."
                                                />
                                            </div>
                                            <div className={classes.rankingName}>
                                                {player.team}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {player.score}
                                    </div>
                                </td>
                                <td className="text-center col-md-1">
                                    <div className={classes.rankingTableRow}>
                                        {player.penalty}
                                    </div>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }

    return (
        <div>
            <Header
                fixed
                color="transparent"
                brand="Vie bóng đá"
                rightLinks={<HeaderLinks />}
                changeColorOnScroll={{
                    height: 80,
                    color: "white",
                }}
                {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <Card className={classes[cardAnimaton]}>
                        <CardHeader color="primary" className={classes.cardHeader}>
                            <h1>Bảng xếp hạng</h1>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={6} lg={6}>
                                    <h2>
                                        TOP GIẢI ĐẤU
                                    </h2>
                                    <CustomTabs
                                        plainTabs
                                        headerColor="danger"
                                        onChange={onRankingTabChange}
                                        tabs={[
                                            {
                                                tabName: 'Premier League',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                            {
                                                tabName: 'La Liga',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                            {
                                                tabName: 'Serie A',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                            {
                                                tabName: 'Bundesliga',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                            {
                                                tabName: 'Ligue 1',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                            {
                                                tabName: 'V-League',
                                                tabContent: (
                                                    renderRankingTable()
                                                ),
                                            },
                                        ]}
                                    />
                                </GridItem>
                                <GridItem xs={12} sm={12} md={6} lg={6}>
                                    <h2>
                                        TOP GHI BÀN
                                    </h2>
                                    <CustomTabs
                                        plainTabs
                                        headerColor="danger"
                                        onChange={onRankingPlayerTabChange}
                                        tabs={[
                                            {
                                                tabName: 'Premier League',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'La Liga',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'Serie A',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'Bundesliga',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'Ligue 1',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'V-League',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'C1',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                            {
                                                tabName: 'C2',
                                                tabContent: (
                                                    renderRankingPlayer()
                                                ),
                                            },
                                        ]}
                                    />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>

                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
