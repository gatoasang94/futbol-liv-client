import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import baseHelper from '../../helpers/BaseHelper';
import { Helmet } from "react-helmet";

import { isEmpty } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import scheduleAction from '../../redux/futures/schedule/actions';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/watchComponent.js";

import image from "assets/img/bg7.png";
import ReactJWPlayer from 'react-jw-player';

import config from '../../config/config';
import { Fragment } from "react";

const useStyles = makeStyles(styles);

export default function WatchComponent(props) {
    let { code } = useParams();

    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");

    const [stream, setStream] = useState({});
    const [timezone, setTimezone] = useState(0);

    const scheduleReducer = useSelector((state) => state.schedule);
    const dispatch = useDispatch();

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    React.useEffect(() => {
        dispatch(scheduleAction.showStream(code));
    }, []);

    React.useEffect(() => {
        if (!scheduleReducer.stream)
            return;

        const stream = scheduleReducer.stream;
        stream.is_living = true;
        // set stream time +7 hour for prerender.io
        if (new Date().getTimezoneOffset() == 0)
            setTimezone(-7);
        setStream(stream);
    }, [scheduleReducer.stream]);

    const classes = useStyles();
    const { ...rest } = props;
    return (
        <div>
            {
                stream && stream.home_team ?
                    <Helmet>
                        <title>Trực tiếp {`${stream.home_team} vs ${stream.away_team} ${baseHelper.getHourFromDate(stream.time, timezone)} ${baseHelper.getDayFromDate(stream.time, timezone)}`} VieBongDa.com</title>
                        <meta name="title" content={`Trực tiếp ${stream.home_team} vs ${stream.away_team} ${baseHelper.getHourFromDate(stream.time, timezone)} ${baseHelper.getDayFromDate(stream.time, timezone)} VieBongDa.com`} />
                        <meta name="og:title" content={`Trực tiếp ${stream.home_team} vs ${stream.away_team} ${baseHelper.getHourFromDate(stream.time, timezone)} ${baseHelper.getDayFromDate(stream.time, timezone)} VieBongDa.com`} />
                        <meta name="og:description" content={`Xem tường thuật trực tiếp ${stream.home_team} vs ${stream.away_team}, lúc ${baseHelper.getHourFromDate(stream.time, timezone)} ${baseHelper.getDayFromDate(stream.time, timezone)} - trên VieBongDa.com - Website xem bóng đá trực tuyến chất lượng cao và ổn định.`} />
                        <meta name="description" content={`Xem tường thuật trực tiếp ${stream.home_team} vs ${stream.away_team}, lúc ${baseHelper.getHourFromDate(stream.time, timezone)} ${baseHelper.getDayFromDate(stream.time, timezone)} - trên VieBongDa.com - Website xem bóng đá trực tuyến chất lượng cao và ổn định.`} />
                        <meta name="og:type" content="article" />
                        <meta name="og:url" content={window.location.href} />
                        <meta name="og:image" content={stream.thumb_url} />
                    </Helmet> : null
            }
            <Header
                absolute
                color="transparent"
                brand="Trực tiếp bóng đá"
                rightLinks={<HeaderLinks />}
                {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={9} lg={9}>
                            <Card className={classes[cardAnimaton]}>
                                <CardHeader color="primary" className={classes.cardHeader}>
                                    <h2 className={classes.teamName}>{stream.home_team} vs {stream.away_team}</h2>
                                    <h2>{`${baseHelper.getHourFromDate(stream.time)} ${baseHelper.getDayFromDate(stream.time)}`}</h2>
                                </CardHeader>
                                <CardBody>
                                    <GridContainer>
                                        <GridItem xs={12} sm={12} md={9} lg={9}>
                                            {
                                                stream.is_living
                                                    ?
                                                    <ReactJWPlayer
                                                        playerId='viebd-player'
                                                        playerScript='https://cdn.jwplayer.com/libraries/ItatZnOE.js'
                                                        // file={'https://' + config.LIVE_HOST + ':2053/live/' + stream.stream_key + '/index.m3u8'} />
                                                        file={'http://45.32.111.29:8888/live/' + stream.stream_key + '/index.m3u8'} />
                                                    :
                                                    stream.is_ended ? ''
                                                        :
                                                        <h2 className={classes.msgStreamOff}>Stream sẽ được cập nhật 15 phút trước giờ bóng lăn!</h2>
                                            }
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={3} lg={3}>
                                            <iframe src="https://www5.cbox.ws/box/?boxid=928681&boxtag=ujDbeD" width="100%" height="100%" allowtransparency="yes" allow="autoplay" frameborder="0" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
                                        </GridItem>
                                    </GridContainer>

                                    {
                                        !isEmpty(stream.links)
                                            ?
                                            <Fragment>
                                                <p className={classes.headerLink}>
                                                    LINK TRỰC TIẾP
                                                </p>
                                                <div className="table-responsive">
                                                    <table className="table table-shopping">
                                                        <thead>
                                                            <tr>
                                                                <th className="text-left" >
                                                                    <div className={classes.headerTableLink}>#</div>
                                                                </th>
                                                                <th className="text-left" >
                                                                    <div className={classes.headerTableLink}>Link</div>
                                                                </th>
                                                                <th className="text-left" >
                                                                    <div className={classes.headerTableLink}>Loại</div>
                                                                </th>
                                                                <th className="text-left">
                                                                    <div className={classes.headerTableLink}>Chất lượng</div>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                stream.links.map((link, index) => (
                                                                    <tr key={index + 1}>
                                                                        <td className="text-left">
                                                                            {index + 1}
                                                                        </td>
                                                                        <td className="text-left">
                                                                            {link.url}
                                                                        </td>
                                                                        <td className="text-left">
                                                                            {link.type}
                                                                        </td>
                                                                        <td className="text-left">
                                                                            {link.quality} kbps
                                                                        </td>
                                                                    </tr>
                                                                ))
                                                            }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Fragment>
                                            :
                                            null
                                    }
                                    {
                                        <div className={classes.boxInfo} dangerouslySetInnerHTML={{ __html: stream.info_html }}></div>
                                    }
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
