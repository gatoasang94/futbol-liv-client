import React from "react";
import ContentLoader from "react-content-loader";

import { makeStyles } from '@material-ui/core/styles';

import GridContainer from 'components/Grid/GridContainer.js';
import GridItem from 'components/Grid/GridItem.js';
import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';

import styles from 'assets/jss/material-kit-react/views/homeStyle.js';

const useStyles = makeStyles(styles);

export default function MatchLoader(props) {

    const classes = useStyles();

    return (
        <GridContainer>
            <GridItem xs={12} sm={12} md={6} lg={6}>
                <Card className={classes.card}>
                    <CardBody>
                        <ContentLoader
                            speed={2}
                            viewBox="0 0 500 110"
                            backgroundColor="#f3f3f3"
                            foregroundColor="#ecebeb"
                            {...props}
                        >
                            <rect x="10" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="420" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="200" y="15" rx="0" ry="0" width="100" height="6" />
                            <rect x="200" y="35" rx="0" ry="0" width="100" height="6" />
                            <rect x="125" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="300" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="215" y="55" rx="0" ry="0" width="70" height="6" />
                            <rect x="200" y="75" rx="0" ry="0" width="100" height="20" />
                        </ContentLoader>
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6} lg={6}>
                <Card className={classes.card}>
                    <CardBody>
                        <ContentLoader
                            speed={2}
                            viewBox="0 0 500 110"
                            backgroundColor="#f3f3f3"
                            foregroundColor="#ecebeb"
                            {...props}
                        >
                            <rect x="10" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="420" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="200" y="15" rx="0" ry="0" width="100" height="6" />
                            <rect x="200" y="35" rx="0" ry="0" width="100" height="6" />
                            <rect x="125" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="300" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="215" y="55" rx="0" ry="0" width="70" height="6" />
                            <rect x="200" y="75" rx="0" ry="0" width="100" height="20" />
                        </ContentLoader>
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6} lg={6}>
                <Card className={classes.card}>
                    <CardBody>
                        <ContentLoader
                            speed={2}
                            viewBox="0 0 500 110"
                            backgroundColor="#f3f3f3"
                            foregroundColor="#ecebeb"
                            {...props}
                        >
                            <rect x="10" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="420" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="200" y="15" rx="0" ry="0" width="100" height="6" />
                            <rect x="200" y="35" rx="0" ry="0" width="100" height="6" />
                            <rect x="125" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="300" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="215" y="55" rx="0" ry="0" width="70" height="6" />
                            <rect x="200" y="75" rx="0" ry="0" width="100" height="20" />
                        </ContentLoader>
                    </CardBody>
                </Card>
            </GridItem>
            <GridItem xs={12} sm={12} md={6} lg={6}>
                <Card className={classes.card}>
                    <CardBody>
                        <ContentLoader
                            speed={2}
                            viewBox="0 0 500 110"
                            backgroundColor="#f3f3f3"
                            foregroundColor="#ecebeb"
                            {...props}
                        >
                            <rect x="10" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="420" y="15" rx="0" ry="0" width="70" height="80" />
                            <rect x="200" y="15" rx="0" ry="0" width="100" height="6" />
                            <rect x="200" y="35" rx="0" ry="0" width="100" height="6" />
                            <rect x="125" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="300" y="55" rx="0" ry="0" width="75" height="6" />
                            <rect x="215" y="55" rx="0" ry="0" width="70" height="6" />
                            <rect x="200" y="75" rx="0" ry="0" width="100" height="20" />
                        </ContentLoader>
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    )
}