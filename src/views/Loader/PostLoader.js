import React from "react";
import ContentLoader from "react-content-loader";

import { makeStyles } from '@material-ui/core/styles';

import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';

import styles from 'assets/jss/material-kit-react/views/homeStyle.js';

const useStyles = makeStyles(styles);

export default function PostLoader(props) {

    const classes = useStyles();

    return (
        <div className={classes.container}>
            <div id="nav-tabs">
                <ContentLoader
                    speed={2}
                    viewBox="0 0 600 180"
                    backgroundColor="#f3f3f3"
                    foregroundColor="#ecebeb"
                    {...props}
                >
                    <rect x="20" y="50" rx="3" ry="3" width="560" height="6" />
                    <rect x="150" y="8" rx="3" ry="3" width="300" height="10" />
                    <rect x="20" y="70" rx="3" ry="3" width="560" height="6" />
                    <rect x="20" y="90" rx="3" ry="3" width="560" height="6" />
                    <rect x="20" y="110" rx="3" ry="3" width="560" height="6" />
                    <rect x="20" y="130" rx="3" ry="3" width="560" height="6" />
                    <rect x="20" y="150" rx="3" ry="3" width="560" height="6" />
                    <rect x="20" y="170" rx="3" ry="3" width="560" height="6" />
                </ContentLoader>
            </div>
        </div>
    )
}