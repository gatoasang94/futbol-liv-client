import React, { useEffect, useState, useImperativeHandle } from 'react';
import { isEmpty, map } from 'lodash';
import { useHistory } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';

// core components
import GridContainer from 'components/Grid/GridContainer.js';
import GridItem from 'components/Grid/GridItem.js';
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import Close from "@material-ui/icons/Close";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import PlayCircleFilled from "@material-ui/icons/PlayCircleFilled";
import Setting from "@material-ui/icons/SettingsOutlined";
// loader
import MatchLoader from '../../Loader/MatchLoader.js';

import '../../../assets/css/material-html/bootstrap.min.css';
import '../../../assets/css/material-html/ggfont.css';
import '../../../assets/css/material-html/material-kit.css';
import styles from 'assets/jss/material-kit-react/views/homeStyle.js';

// image
import defaultTeamIcon from 'assets/img/football_icon.png';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import scheduleAction from '../../../redux/futures/schedule/actions';

// type
import * as scheduleType from '../../../redux/futures/schedule/types';

import baseHelper from '../../../helpers/BaseHelper';

import appConfig from '../../../config/app';
import config from '../../../config/config';

import { appLocalStorage } from '../../../localforage';
import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';
import LazyLoad from 'react-lazyload';

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

const MainSection = React.forwardRef((props, ref) => {
    const history = useHistory();
    const classes = useStyles();

    const { data } = props;

    const [streams, setStreams] = useState([]);
    const [openSettingStreamModal, setOpenSettingStreamModal] = React.useState(false);
    const [currentStream, setCurrentStream] = useState({});
    const [isAdmin, setIsAdmin] = useState(false);
    const [linkInfo, setLinkInfo] = useState({ type: '', quality: '', url: '' });

    const scheduleReducer = useSelector((state) => state.schedule);
    const dispatch = useDispatch();

    useImperativeHandle(ref, () => ({
        showSettingStream(id) {
            dispatch(scheduleAction.showSettingStream(id));
        }
    }));

    // invoke once if second argument is empty array
    useEffect(() => {
        // set role
        appLocalStorage.getItem(appConfig.LOCAL_FORAGE.USER_INFO)
            .then(data => {
                try {
                    const user = data;

                    if (user.role === 'ADMIN')
                        setIsAdmin(true);
                    else
                        setIsAdmin(false);
                } catch (e) { }
            });
    }, []);

    useEffect(() => {
        // set role
        appLocalStorage.getItem(appConfig.LOCAL_FORAGE.USER_INFO)
            .then(data => {
                try {
                    const user = data;

                    if (user.role === 'ADMIN')
                        setIsAdmin(true);
                    else
                        setIsAdmin(false);
                } catch (e) { setIsAdmin(false); }
            });

        setStreams(data || []);
    }, [data]);

    useEffect(() => {
        if (scheduleReducer.stream && scheduleReducer.action == scheduleType.SHOW_SETTING_STREAM) {
            setOpenSettingStreamModal(true);
            setCurrentStream(scheduleReducer.stream);
        }

        if (scheduleReducer.action === scheduleType.ADD_STREAM_LINK) {
            setLinkInfo({ type: '', quality: '', url: '' });
        }
    }, [scheduleReducer.stream]);

    useEffect(() => {
        if (scheduleReducer.action == scheduleType.UPDATE_ORDER) {
            if (scheduleReducer.status)
                window.FBL.toastr('success', '', scheduleReducer.message);
            else
                window.FBL.toastr('error', '', scheduleReducer.message);
        }

        if (scheduleReducer.action == scheduleType.UPDATE_THUMB) {
            if (scheduleReducer.status)
                window.FBL.toastr('success', '', scheduleReducer.message);
            else
                window.FBL.toastr('error', '', scheduleReducer.message);
        }
    }, [scheduleReducer]);

    const onChangeLinkQuality = (e) => {
        if (!e || !e.target)
            return;

        const data = { ...linkInfo };
        data.quality = e.target.value;
        setLinkInfo(data);
    }

    const onChangeLinkType = (e) => {
        if (!e || !e.target)
            return;

        const data = { ...linkInfo };
        data.type = e.target.value;
        setLinkInfo(data);
    }

    const onChangeLinkUrl = (e) => {
        if (!e || !e.target)
            return;

        const data = { ...linkInfo };
        data.url = e.target.value;
        setLinkInfo(data);
    }

    const showSettingStream = (id) => {
        dispatch(scheduleAction.showSettingStream(id));
    }

    const renderNav = () => {
        const filter = props.filter;
        return (
            <div className={classes.filterContainer}>
                <Button
                    className={classes.btnFilter}
                    onClick={e => { e.preventDefault(); props.onChangeFilter('all') }}
                    color={filter == 'all' ? 'rose' : 'transparent'}
                    round
                >
                    Tất cả
                </Button>
                <Button
                    className={classes.btnFilter}
                    onClick={e => { e.preventDefault(); props.onChangeFilter('hot') }}
                    color={filter == 'hot' ? 'rose' : 'transparent'}
                    round
                >
                    Trận HOT
                </Button>
                <Button
                    className={classes.btnFilter}
                    onClick={e => { e.preventDefault(); props.onChangeFilter('now') }}
                    color={filter == 'now' ? 'rose' : 'transparent'}
                    round
                >
                    Đang đá
                </Button>
                <Button
                    className={classes.btnFilter}
                    onClick={e => { e.preventDefault(); props.onChangeFilter('live') }}
                    color={filter == 'live' ? 'danger' : 'transparent'}
                    round
                >
                    Đang live
                </Button>
            </div>
        )
    }

    const renderDataColumn = () => {
        return (
            map(streams, (stream, index) => {
                return (
                    <GridItem xs={12} sm={12} md={6} lg={6} key={index} className={classes.grid}>
                        <Card className={classes.card}>
                            <CardBody>
                                <GridContainer>
                                    <GridItem xs={3} sm={2} md={2} lg={2} className={classes.logoBox}>
                                        <img
                                            id="team-logo-stream"
                                            className={classes.teamLogoStream}
                                            src={baseHelper.getTeamImageFromName(stream.home_team) || defaultTeamIcon}
                                            alt="..."
                                            width='60'
                                            height='60'
                                        />
                                    </GridItem>
                                    <GridItem xs={6} sm={8} md={8} lg={8} className={classes.infoBox}>
                                        <GridContainer>
                                            <GridItem xs={12} sm={12} md={12} lg={12} className={classes.leagueName}>
                                                {baseHelper.getSortLeagueName(stream.league)}
                                            </GridItem>
                                            <GridItem xs={12} sm={12} md={12} lg={12} className={classes.streamTime}>
                                                <p style={{ color: 'red', paddingRight: '10px', fontWeight: 'bold' }}>{stream.live_time || ''}</p>
                                                <p>
                                                    {
                                                        `${baseHelper.getHourFromDate(stream.time)} ${baseHelper.getDayFromDate(stream.time)}`
                                                    }
                                                </p>
                                            </GridItem>
                                            <GridItem xs={12} sm={12} md={12} lg={12}>
                                                <GridContainer>
                                                    <GridItem xs={12} sm={5} md={5} lg={5} className={classes.scoreBoard}>
                                                        <p title={stream.home_team}>
                                                            {stream.home_team}
                                                        </p>
                                                    </GridItem>
                                                    <GridItem xs={12} sm={2} md={2} lg={2} className={classes.score}>
                                                        <p style={{ margin: '0px', color: 'red' }}>{baseHelper.getTimeString(stream.live_score) || stream.score}</p>
                                                    </GridItem>
                                                    <GridItem xs={12} sm={5} md={5} lg={5} className={classes.scoreBoard}>
                                                        <p title={stream.away_team}>
                                                            {stream.away_team}
                                                        </p>
                                                    </GridItem>
                                                </GridContainer>
                                            </GridItem>
                                        </GridContainer>
                                        <div className={classes.buttonBox}>
                                            {
                                                isAdmin
                                                    ?
                                                    <Button
                                                        className={`btn btn-primary btn-round btn-lg btn-warning ${classes.btnAction}`}
                                                        onClick={e => { showSettingStream(stream); }}>
                                                        <Setting /> Cài đặt
                                                    </Button>
                                                    :
                                                    null
                                            }
                                            <Button
                                                className={`btn btn-round btn-lg ${stream.is_living ? `btn-danger btn-primary` : ``} ${classes.btnAction}`}
                                                onClick={e => history.push(`/watch/${stream.code}`)}>
                                                <PlayCircleFilled /> {`${stream.is_living ? `Xem trực tiếp` : `Thông tin`}`}
                                            </Button>
                                        </div>
                                    </GridItem>
                                    <GridItem xs={3} sm={2} md={2} lg={2} className={classes.logoBox}>
                                        <img
                                            id="team-logo-stream"
                                            className={classes.teamLogoStream}
                                            src={baseHelper.getTeamImageFromName(stream.away_team) || defaultTeamIcon}
                                            alt="..."
                                            width='60'
                                            height='60'
                                        />
                                    </GridItem>
                                </GridContainer>
                            </CardBody>
                        </Card>
                    </GridItem>
                )
            })
        );
    }

    return (
        <div className={classes.section}>
            <div className={classes.container} style={{ textAlign: '-webkit-center' }}>
                <div id="nav-tabs" className={classes.matchWrapper}>
                    {
                        props.isHome && renderNav()
                    }
                    {
                        props.isLoading
                            ?
                            <MatchLoader />
                            :
                            <LazyLoad>
                                {
                                    isEmpty(streams)
                                        ?
                                        <div>
                                            Không có trận đấu nào.
                                        </div>
                                        :
                                        <GridContainer>
                                            {
                                                renderDataColumn()
                                            }
                                        </GridContainer>
                                }
                            </LazyLoad>
                    }
                </div>
            </div>
            {
                isAdmin &&
                <Dialog
                    classes={{
                        root: classes.center,
                        paper: classes.modal
                    }}
                    open={openSettingStreamModal}
                    TransitionComponent={Transition}
                    keepMounted
                    onClose={() => setOpenSettingStreamModal(false)}
                    aria-labelledby="classic-modal-slide-title"
                    aria-describedby="classic-modal-slide-description"
                    maxWidth="xl"
                >
                    <DialogTitle
                        id="classic-modal-slide-title"
                        disableTypography
                        className={classes.modalHeader}
                    >
                        <IconButton
                            className={classes.modalCloseButton}
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={() => setOpenSettingStreamModal(false)}
                        >
                            <Close className={classes.modalClose} />
                        </IconButton>
                        <h4 className={classes.modalTitle}>Cài đặt stream</h4>
                    </DialogTitle>
                    <DialogContent
                        id="classic-modal-slide-description"
                        className={classes.modalBody}
                    >
                        <p className={classes.modalIns}>
                            Server: <b>rtmp://{config.STREAM_HOST}:1935/live</b>
                        </p>
                        <p className={classes.modalIns}>
                            Stream Key: <b>{currentStream.stream_key}</b>
                        </p>
                        <p className={classes.modalIns}>
                            Thumb URL: <b>{currentStream.thumb_url}</b>
                        </p>
                        <GridContainer>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <CustomInput
                                    inputProps={{
                                        placeholder: "Loại",
                                        value: linkInfo.type,
                                        onChange: onChangeLinkType
                                    }}
                                    formControlProps={{
                                        fullWidth: true
                                    }}
                                />
                            </GridItem>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <CustomInput
                                    inputProps={{
                                        placeholder: "Chất lượng",
                                        type: "number",
                                        value: linkInfo.quality,
                                        onChange: onChangeLinkQuality
                                    }}
                                    formControlProps={{
                                        fullWidth: true,
                                    }}
                                />
                            </GridItem>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <CustomInput
                                    inputProps={{
                                        placeholder: "Link",
                                        value: linkInfo.url,
                                        onChange: onChangeLinkUrl
                                    }}
                                    formControlProps={{
                                        fullWidth: true
                                    }}
                                />
                            </GridItem>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <Button
                                    className={classes.buttonAddLink}
                                    onClick={() => dispatch(scheduleAction.addStreamLink(currentStream._id, linkInfo))}
                                >
                                    Thêm
                                </Button>
                            </GridItem>
                        </GridContainer>
                        <GridContainer>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <CustomInput
                                    inputProps={{
                                        placeholder: "Thứ tự ưu tiên",
                                        type: "number",
                                        value: currentStream.order,
                                        onChange: (e) => { e.preventDefault(); setCurrentStream({ ...currentStream, order: e.target.value }) }
                                    }}
                                    formControlProps={{
                                        fullWidth: true,
                                    }}
                                />
                            </GridItem>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <Button
                                    className={classes.buttonAddLink}
                                    onClick={() => dispatch(scheduleAction.updateOrder(currentStream._id, currentStream.order))}
                                >
                                    Lưu
                                </Button>
                            </GridItem>
                        </GridContainer>
                        <GridContainer>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <CustomInput
                                    inputProps={{
                                        placeholder: "Đường dẫn ảnh thumb",
                                        value: currentStream.thumb_url,
                                        onChange: (e) => { e.preventDefault(); setCurrentStream({ ...currentStream, thumb_url: e.target.value }) }
                                    }}
                                    formControlProps={{
                                        fullWidth: true,
                                    }}
                                />
                            </GridItem>
                            <GridItem xs={12} sm={3} md={3} lg={3}>
                                <Button
                                    className={classes.buttonAddLink}
                                    onClick={() => dispatch(scheduleAction.updateThumbURL(currentStream._id, currentStream.thumb_url))}
                                >
                                    Lưu
                                </Button>
                            </GridItem>
                        </GridContainer>
                        <p className={classes.headerSetupLink}>
                            LINK TRỰC TIẾP
                        </p>
                        <div className="table-responsive">
                            <table className="table table-shopping">
                                <thead>
                                    <tr>
                                        <th className="text-left" >
                                            <div className={classes.headerTableLink}>#</div>
                                        </th>
                                        <th className="text-left" >
                                            <div className={classes.headerTableLink}>Link</div>
                                        </th>
                                        <th className="text-left" >
                                            <div className={classes.headerTableLink}>Loại</div>
                                        </th>
                                        <th className="text-left">
                                            <div className={classes.headerTableLink}>Chất lượng</div>
                                        </th>
                                        <th />
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        !isEmpty(currentStream.links)
                                            ?
                                            currentStream.links.map((link, index) => (
                                                <tr key={index + 1}>
                                                    <td className="text-left">
                                                        {index + 1}
                                                    </td>
                                                    <td className="text-left">
                                                        {link.url}
                                                    </td>
                                                    <td className="text-left">
                                                        {link.type}
                                                    </td>
                                                    <td className="text-left">
                                                        {link.quality} kbps
                                                    </td>
                                                    <td className="text-center">
                                                    </td>
                                                    <td className="td-actions text-right">
                                                        <button type="button" rel="tooltip" className="btn btn-danger" style={{ margin: '5px' }}
                                                            onClick={e => dispatch(scheduleAction.deleteStreamLink(currentStream._id, link.url))}>
                                                            <i className="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))
                                            :
                                            <tr>
                                                <td>
                                                    <p className={classes.notFoundText}>Không tìm link nào!</p>
                                                </td>
                                            </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </DialogContent>
                    <DialogActions className={classes.modalFooter}>
                        <Button
                            onClick={() => setOpenSettingStreamModal(false)}
                            color="danger"
                            simple
                        >
                            Đóng
                        </Button>
                    </DialogActions>
                </Dialog>
            }
        </div >
    );
})

export default MainSection;
