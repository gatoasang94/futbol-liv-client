import React, { useEffect, useState } from 'react';
import { isEmpty, map } from 'lodash';
import { useHistory } from "react-router-dom";
// @material-ui/core components
import { makeStyles } from '@material-ui/core/styles';

// core components
import GridContainer from 'components/Grid/GridContainer.js';
import GridItem from 'components/Grid/GridItem.js';
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import Slide from "@material-ui/core/Slide";
import Close from "@material-ui/icons/Close";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import PlayCircleFilled from "@material-ui/icons/PlayCircleFilled";
import Setting from "@material-ui/icons/SettingsOutlined";
import Pagination from "components/Pagination/Pagination.js";

import '../../../assets/css/material-html/bootstrap.min.css';
import '../../../assets/css/material-html/ggfont.css';
import '../../../assets/css/material-html/material-kit.css';
import styles from 'assets/jss/material-kit-react/views/homeStyle.js';

// image
import defaultTeamIcon from 'assets/img/football_icon.png';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import scheduleAction from '../../../redux/futures/schedule/actions';
import teamAction from '../../../redux/futures/team/actions';

// type
import * as scheduleType from '../../../redux/futures/schedule/types';

import baseHelper from '../../../helpers/BaseHelper';

import appConfig from '../../../config/app';
import config from '../../../config/config';

import { appLocalStorage } from '../../../localforage';
import Card from 'components/Card/Card';
import CardBody from 'components/Card/CardBody';
import CardHeader from "components/Card/CardHeader.js";
import PostLoader from 'views/Loader/PostLoader';

// action
import action from '../../../redux/futures/post/actions';

// type
import * as postType from '../../../redux/futures/post/types';

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

export default function PostSection(props) {
    const reducer = useSelector((state) => state.post);
    const history = useHistory();
    const classes = useStyles();

    const { data } = props;
    const [isAdmin, setIsAdmin] = useState(false);
    const [posts, setPosts] = useState([]);
    const [linkInfo, setLinkInfo] = useState({ type: '', quality: '', url: '' });
    const [currentPage, setCurrentPage] = useState(1);
    const [totalResult, setTotalResult] = useState(1);
    const [postFilter, setPostFilter] = useState(appConfig.POST.CATEGORY.LASTEST.name);

    const dispatch = useDispatch();

    // invoke once if second argument is empty array
    useEffect(() => {
        // set role
        appLocalStorage.getItem(appConfig.LOCAL_FORAGE.USER_INFO)
            .then(data => {
                try {
                    const user = data;

                    if (user.role === 'ADMIN')
                        setIsAdmin(true);
                    else
                        setIsAdmin(false);
                } catch (e) { }
            });

        setTimeout(() => {
            dispatch(action.list());
        }, 1000);
    }, []);

    useEffect(() => {
        setPosts(data || []);
    }, [data]);

    useEffect(() => {
        if (reducer.action === postType.LIST && reducer.posts) {
            setPosts(reducer.posts);
            setCurrentPage(reducer.page);
            setTotalResult(reducer.total);
        }
    }, [reducer])

    const renderNav = () => {
        return (
            <div style={{ padding: '0 10px 0 10px' }}>
                <Button
                    onClick={e => { e.preventDefault(); onChangeFilter(appConfig.POST.CATEGORY.LASTEST.name) }}
                    color={postFilter == appConfig.POST.CATEGORY.LASTEST.name ? 'primary' : 'transparent'}
                    round
                >
                    Mới nhất
                </Button>
                <Button
                    onClick={e => { e.preventDefault(); onChangeFilter(appConfig.POST.CATEGORY.VIDEO.name) }}
                    color={postFilter == appConfig.POST.CATEGORY.VIDEO.name ? 'primary' : 'transparent'}
                    round
                >
                    Video
                </Button>
                <Button
                    onClick={e => { e.preventDefault(); onChangeFilter(appConfig.POST.CATEGORY.NEWS.name) }}
                    color={postFilter == appConfig.POST.CATEGORY.NEWS.name ? 'primary' : 'transparent'}
                    round
                >
                    Tin tức
                </Button>
            </div>
        )
    }

    const onChangeFilter = (type) => {
        if (postFilter == type)
            return;
        dispatch(action.list({category: type}));
        setPostFilter(type);
    }

    const changeTablePage = (e, text) => {
        let newPage = currentPage;
        if (text == 'PREV' && currentPage > 1)
            newPage = currentPage - 1;
        else if (text == 'NEXT' && totalResult > currentPage * 10)
            newPage = currentPage + 1;
        else
            newPage = parseInt(text);

        if (newPage != currentPage)
            dispatch(action.list({ category: postFilter, page: newPage }))
    }

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10} lg={10} className={classes.postWrapper}>
                    {
                        props.isHome && renderNav()
                    }
                    {
                        isEmpty(posts)
                            ?
                            <div>Không tìm thấy bài viết nào.</div>
                            :
                            <>
                                <GridContainer style={{ margin: '0px' }}>
                                    {
                                        map(posts, (post, index) => {
                                            return (
                                                <GridItem xs={12} sm={6} md={6} lg={3} xl={3} key={index}>
                                                    <Card plain style={{ marginBottom: '0px' }} >
                                                        <CardHeader image plain>
                                                            <a onClick={() => { history.push(`/post/${post.code}`) }} className={classes.imgBox}>
                                                                <img src={post.thumb} alt="..." className={classes.imgThumb} width='180' height='120' />
                                                            </a>
                                                            <div
                                                                className={classes.coloredShadow}
                                                                style={{
                                                                    backgroundImage: "url(" + post.thumb + ")",
                                                                    opacity: "1"
                                                                }}
                                                            />
                                                        </CardHeader>
                                                        <CardBody plain>
                                                            <h4 className={classes.category}>{post.category}</h4>
                                                            <h3 className={classes.cardTitle}>
                                                                <a onClick={() => { history.push(`/post/${post.code}`) }}>
                                                                    {post.title}
                                                                </a>
                                                            </h3>
                                                            {/* <h5 style={{ fontStyle: 'italic', opacity: '0.8' }}>{baseHelper.formatStrToDate(post.created_at)}</h5> */}
                                                            {/* <div className={classes.description} dangerouslySetInnerHTML={{ __html: `<p style="font-size: 13px">${post.brief}</p>` }}></div> */}
                                                        </CardBody>
                                                    </Card>
                                                </GridItem>
                                            )
                                        })
                                    }
                                </GridContainer>
                                <Pagination
                                    onClick={changeTablePage}
                                    total={totalResult}
                                    currentPage={currentPage}
                                    color="info"
                                />
                            </>
                    }
                </GridItem>
            </GridContainer>
        </div>
    );
}
