import { isEmpty, map } from 'lodash';
import appConfig from '../../config/app';
import React, { useEffect, useState, useRef } from 'react';
import { useHistory } from "react-router-dom";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
import PlayCircleFilled from "@material-ui/icons/PlayCircleFilled";
import Setting from "@material-ui/icons/SettingsOutlined";
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";
import Parallax from "components/Parallax/Parallax.js";
// sections for this page
import HeaderLinks from "components/Header/HeaderLinks.js";
import MainSection from "./Sections/MainSection.js";
import PostSection from "./Sections/PostSection.js";
// css
import styles from "assets/jss/material-kit-react/views/components.js";

// redux
import { useDispatch, useSelector } from 'react-redux';
import defaultTeamIcon from 'assets/img/football_icon.png';

// action
import scheduleAction from '../../redux/futures/schedule/actions';
import * as types from '../../redux/futures/schedule/types';
import * as postTypes from '../../redux/futures/post/types';
import postAction from '../../redux/futures/post/actions';
import CardBody from 'components/Card/CardBody.js';
import Card from 'components/Card/Card.js';
import LazyLoad from 'react-lazyload';
import { Helmet } from "react-helmet";

import baseHelper from '../../helpers/BaseHelper';
import { appLocalStorage } from '../../localforage';


const useStyles = makeStyles(styles);

export default function HomePage(props) {
    const history = useHistory();
    const classes = useStyles();
    const { ...rest } = props;

    const mainSectionRef = useRef(null);

    const scheduleReducer = useSelector((state) => state.schedule);
    const dispatch = useDispatch();

    const [streams, setStreams] = useState([]);
    const [hotStreams, setHotStreams] = useState([]);
    const [filter, setFilter] = useState("all");
    const [isAdmin, setIsAdmin] = useState(false);

    useEffect(() => {
        dispatch(scheduleAction.getLiveStream(filter));

        // set role
        appLocalStorage.getItem(appConfig.LOCAL_FORAGE.USER_INFO)
            .then(data => {
                try {
                    const user = data;

                    if (user.role === 'ADMIN')
                        setIsAdmin(true);
                    else
                        setIsAdmin(false);
                } catch (e) { }
            });
    }, []);

    useEffect(() => {
        setStreams(scheduleReducer.streams || []);
    }, [scheduleReducer.streams]);

    useEffect(() => {
        setHotStreams(scheduleReducer.hotStreams || []);
    }, [scheduleReducer.hotStreams]);

    const onChangeFilter = (type) => {
        if (filter == type)
            return;
        dispatch(scheduleAction.getLiveStream(type));
        setFilter(type);
    }

    return (
        <div>
            {
                <Helmet>
                    <title>Trực tiếp bóng đá chất lượng cao hàng đầu Việt Nam</title>
                    <meta name='title' content='Trực tiếp bóng đá chất lượng cao hàng đầu Việt Nam' />
                    <meta name='og:title' content='Trực tiếp bóng đá chất lượng cao hàng đầu Việt Nam' />
                    <meta name='og:description' content='Trực tiếp bóng đá chất lượng cao hàng đầu Việt Nam' />
                    <meta name='description' content='Trực tiếp bóng đá chất lượng cao hàng đầu Việt Nam' />
                    <meta name='og:type' content=' article' />
                    <meta name='og:image' content='https://viebongda.com/images/home_thumb.JPG' />
                </Helmet>
            }
            <Header
                brand="VIE BÓNG ĐÁ"
                rightLinks={<HeaderLinks action={() => { setStreams([...streams]) }} />}
                fixed
                color="transparent"
                changeColorOnScroll={{
                    height: 200,
                    color: "white",
                }}
                {...rest}
            />

            <LazyLoad>
                <div style={{ display: 'flex' }}>
                    <Parallax image={require("assets/img/stadium3.jpg")}>
                        {
                            hotStreams.length > 0
                                ?
                                <div className={classes.topMatchContainer}>
                                    <div className={classes.countdown}>
                                        <h3 className={classes.leagueName}>{hotStreams[0].league.toUpperCase()}</h3>
                                        <div className={classes.timeContainer}>
                                            <p className={classes.matchTime}>{hotStreams[0].live_time || ''}</p>
                                            <p className={classes.matchDate}>
                                                {
                                                    `${baseHelper.getHourFromDate(hotStreams[0].time)} ${baseHelper.getDayFromDate(hotStreams[0].time)}`
                                                }
                                            </p>
                                        </div>
                                        <div className={classes.matchTeam}>
                                            <div className={classes.matchItem}>
                                                <img
                                                    id="team-logo-stream"
                                                    className={classes.teamLogoStream}
                                                    src={baseHelper.getTeamImageFromName(hotStreams[0].home_team) || defaultTeamIcon}
                                                    alt="..."
                                                    width='100'
                                                    height='100'
                                                />
                                                <p title={hotStreams[0].home_team} className={classes.teamName}>
                                                    {hotStreams[0].home_team.toUpperCase()}
                                                </p>
                                            </div>
                                            <div className={classes.matchItem} style={{ alignSelf: 'center', fontSize: '50px', fontWeight: '500' }}>{baseHelper.getTimeString(hotStreams[0].live_score) || hotStreams[0].score}</div>
                                            <div className={classes.matchItem}>
                                                <img
                                                    id="team-logo-stream"
                                                    className={classes.teamLogoStream}
                                                    src={baseHelper.getTeamImageFromName(hotStreams[0].away_team) || defaultTeamIcon}
                                                    alt="..."
                                                    width='100'
                                                    height='100'
                                                />
                                                <p title={hotStreams[0].away_team} className={classes.teamName}>
                                                    {hotStreams[0].away_team.toUpperCase()}
                                                </p>
                                            </div>
                                        </div>
                                        <div className={classes.buttonBox}>
                                            {
                                                isAdmin && mainSectionRef
                                                    ?
                                                    <Button
                                                        style={{ marginRight: '10px' }}
                                                        className={`btn btn-primary btn-round btn-lg btn-warning ${classes.btnAction}`}
                                                        onClick={e => { mainSectionRef.current.showSettingStream(hotStreams[0]._id); }}>
                                                        <Setting /> Cài đặt
                                                    </Button>
                                                    :
                                                    null
                                            }
                                            <Button
                                                className={`btn btn-round btn-black btn-lg ${hotStreams[0].is_living ? `btn-danger btn-primary` : ``} ${classes.btnAction}`}
                                                onClick={e => history.push(`/watch/${hotStreams[0].code}`)}>
                                                <PlayCircleFilled /> {`${hotStreams[0].is_living ? `Xem trực tiếp` : `Thông tin`}`}
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }
                    </Parallax>
                </div>
            </LazyLoad>

            <div className={classNames(classes.main, classes.mainRaised)}>
                {
                    <MainSection
                        isHome={true}
                        filter={filter}
                        data={streams}
                        isLoading={scheduleReducer.loading && scheduleReducer.waitting == types.GET_LIVE_STREAM}
                        ref={mainSectionRef}
                        onChangeFilter={onChangeFilter} />
                }
                <PostSection
                    isHome={true} />
            </div>
            <Footer />
        </div>
    );
}
