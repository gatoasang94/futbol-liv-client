import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Slide from "@material-ui/core/Slide";
import PostLoader from '../Loader/PostLoader.js';

import baseHelper from '../../helpers/BaseHelper';

import { isEmpty, filter, forEach } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import postAction from '../../redux/futures/post/actions';

// type
import * as postType from '../../redux/futures/post/types';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/postComponent.js";

import image from "assets/img/bg7.png";

import appConfig from '../../config/app';
import { Fragment } from "react";

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

export default function PostComponent(props) {
    let { code } = useParams();
    const { ...rest } = props;

    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const postReducer = useSelector((state) => state.post);
    const dispatch = useDispatch();

    const classes = useStyles();

    const [post, setPost] = useState({});

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    useEffect(() => {
        dispatch(postAction.show(code));
    }, []);

    useEffect(() => {
        if (postReducer.status && postReducer.action === postType.SHOW_POST)
            setPost(postReducer.post)
    }, [postReducer.post]);

    return (
        <div>
            <Header
                fixed
                color="transparent"
                brand="Vie bóng đá"
                rightLinks={<HeaderLinks />}
                changeColorOnScroll={{
                    height: 80,
                    color: "white",
                }}
                {...rest}
            />
            <div className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}>
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={10} md={6} lg={6}>
                            <Card className={classes[cardAnimaton]}>
                                <CardBody>
                                    {
                                        postReducer.loading
                                            ?
                                            <PostLoader />
                                            :
                                            <Fragment>
                                                <div className={classes.titleBox}>
                                                    <p className={classes.title}>
                                                        {post.title}
                                                    </p>
                                                </div>
                                                {
                                                    <div className={classes.content} dangerouslySetInnerHTML={{ __html: post.content }}></div>
                                                }
                                            </Fragment>
                                    }
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
