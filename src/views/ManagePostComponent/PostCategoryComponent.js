import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Slide from "@material-ui/core/Slide";

import PostSection from '../HomePage/Sections/PostSection';

import baseHelper from '../../helpers/BaseHelper';

import { isEmpty, filter, forEach } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import postAction from '../../redux/futures/post/actions';

// type
import * as postType from '../../redux/futures/post/types';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/managePostComponent.js";

import image from "assets/img/bg7.png";

import appConfig from '../../config/app';

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

export default function PostCategoryComponent(props) {
    let { category } = useParams();

    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const postReducer = useSelector((state) => state.post);
    const dispatch = useDispatch();

    const classes = useStyles();
    const { ...rest } = props;

    const [title, setTitle] = useState('');
    const [posts, setPosts] = useState([]);

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    useEffect(() => {
        let _category;

        switch (category) {
            case appConfig.POST.CATEGORY.VIDEO.alias:
                _category = appConfig.POST.CATEGORY.VIDEO.name;
                break;
            case appConfig.POST.CATEGORY.NEWS.alias:
                _category = appConfig.POST.CATEGORY.NEWS.name;
                break;
            case appConfig.POST.CATEGORY.COMMENT.alias:
                _category = appConfig.POST.CATEGORY.COMMENT.name;
                break;
            case appConfig.POST.CATEGORY.TRANSFER.alias:
                _category = appConfig.POST.CATEGORY.TRANSFER.name;
                break;
            case appConfig.POST.CATEGORY.FOOTBALL_HISTORY.alias:
                _category = appConfig.POST.CATEGORY.FOOTBALL_HISTORY.name;
                break;
            default:
                break;
        }

        dispatch(postAction.getPosts(_category));
        setTitle(_category);
    }, [category]);

    useEffect(() => {
        if (postReducer.posts)
            setPosts(postReducer.posts);
    }, [postReducer.posts]);

    return (
        <div>
            <Header
                fixed
                color="transparent"
                brand="Vie bóng đá"
                rightLinks={<HeaderLinks />}
                changeColorOnScroll={{
                    height: 80,
                    color: "white",
                }}
                {...rest}
            />
            <div className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}>
                <div className={classes.container}>
                    <Card className={classes[cardAnimaton]}>
                        <CardBody>
                            <PostSection data={posts} />
                        </CardBody>
                    </Card>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
