import { useParams } from "react-router";
import React, { useEffect, useState } from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import People from "@material-ui/icons/People";
// core components
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import CustomTabs from 'components/CustomTabs/CustomTabs.js';
import MainSection from "../HomePage/Sections/MainSection.js";
import NavPills from "components/NavPills/NavPills.js";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Slide from "@material-ui/core/Slide";
import IconButton from "@material-ui/core/IconButton";
import Close from "@material-ui/icons/Close";
import PlusIcon from "@material-ui/icons/Add";
import Search from "@material-ui/icons/Search";
import Edit from "@material-ui/icons/Edit";
import Visibility from "@material-ui/icons/Visibility";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import { Editor } from '@tinymce/tinymce-react';
import baseHelper from '../../helpers/BaseHelper';

import { isEmpty, filter, forEach } from 'lodash';

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import postAction from '../../redux/futures/post/actions';

// type
import * as postType from '../../redux/futures/post/types';

import '../../assets/css/material-html/bootstrap.min.css';
import '../../assets/css/material-html/ggfont.css';
import '../../assets/css/material-html/material-kit.css';
import styles from "assets/jss/material-kit-react/views/managePostComponent.js";

import image from "assets/img/bg7.png";

import appConfig from '../../config/app';

const useStyles = makeStyles(styles);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

export default function ManagePostComponent(props) {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    const postReducer = useSelector((state) => state.post);
    const dispatch = useDispatch();

    const classes = useStyles();
    const { ...rest } = props;

    const [editorKey, setEditorKey] = useState(0);
    const [mediaUrl, setMediaUrl] = useState('');
    const [openModal, setOpenModal] = useState(false);
    const [openModalConfirm, setOpenModalConfirm] = useState(false);
    const [post, setPost] = useState({ title: '', thumb: '' });
    const [posts, setPosts] = useState([]);
    const [keyword, setKeyword] = useState('');

    setTimeout(function () {
        setCardAnimation("");
    }, 700);

    useEffect(() => {
        dispatch(postAction.getPosts());
    }, []);

    useEffect(() => {
        if (postReducer.action === postType.SAVE_POST) {
            if (postReducer.status) {
                if (postReducer.post.is_create)
                    posts.unshift(postReducer.post);
                else {
                    forEach(posts, p => {
                        if (p._id === post._id) {
                            p.title = postReducer.post.title;
                            p.content = postReducer.post.content;
                            p.thumb = postReducer.post.thumb;
                            p.category = postReducer.post.category;
                        }
                    });
                }

                setPosts(posts);
                setPost({ title: '', thumb: '' });
                setOpenModal(false);

                setNewEditorKey();

                window.FBL.toastr('success', '', postReducer.message);
            } else
                window.FBL.toastr('error', '', postReducer.message);
        }

        if (postReducer.action === postType.GET_POSTS) {
            if (postReducer.status && postReducer.posts)
                setPosts(postReducer.posts);
        }

        if (postReducer.action === postType.DELETE_POST) {
            if (postReducer.status) {
                const temp = filter(posts, p => p._id !== post._id);
                setPosts(temp);
                setOpenModalConfirm(false);
            } else
                window.FBL.toastr('error', '', postReducer.message);
        }

        if (postReducer.action === postType.SEARCH_POST) {
            if (postReducer.status && postReducer.posts)
                setPosts(postReducer.posts);
        }
    }, [postReducer]);

    const onChangeTitle = (e) => {
        if (!e || !e.target)
            return;

        const data = { ...post };
        data.title = e.target.value;
        setPost(data);
    }

    const onChangeThumb = (e) => {
        if (!e || !e.target)
            return;

        const data = { ...post };
        data.thumb = e.target.value;
        setPost(data);
    }

    const onChangeKeyword = (e) => {
        if (!e || !e.target)
            return;

        setKeyword(e.target.value);
    }

    const onSelectCategory = (key) => {
        const data = { ...post };
        data.category = key;
        setPost(data);
    }

    const handleEditorChange = (content) => {
        const data = { ...post };
        data.content = content;
        setPost(data);
    }

    const setNewEditorKey = () => {
        setEditorKey(editorKey * 2 + 1);
    }

    const savePost = () => {
        // validate
        if (isEmpty(post.title)) {
            window.FBL.toastr('warning', '', 'Tiêu đề không được để trống');
            return;
        }
        if (isEmpty(post.category)) {
            window.FBL.toastr('warning', '', 'Chuyên mục không được để trống');
            return;
        }
        if (isEmpty(post.content)) {
            window.FBL.toastr('warning', '', 'Nội dung không được để trống');
            return;
        }

        // set thumb
        if (isEmpty(post.thumb) && mediaUrl) {
            const videoID = mediaUrl.split('v=')[1];
            var ampersandPosition = videoID.indexOf('&');
            if (ampersandPosition != -1) {
                videoID = videoID.substring(0, ampersandPosition);
            }

            post.thumb = `https://img.youtube.com/vi/${videoID}/hqdefault.jpg`;
        }

        dispatch(postAction.savePost(post));
    }

    const prepareDeletePost = (post) => {
        setPost(post);
        setOpenModalConfirm(true);
    }

    const prepareUpdatePost = (post) => {
        setPost(post);
        setOpenModal(true);
        setNewEditorKey();
    }

    const viewPost = (post) => {
        window.open(`https://viebongda.com/post/${post.code}`, "_blank")
    }

    const prepareAddPost = () => {
        setOpenModal(true);
        setPost({ title: '', thumb: '' })
        setNewEditorKey();
    }

    return (
        <div>
            <Header
                fixed
                color="transparent"
                brand="Vie bóng đá"
                rightLinks={<HeaderLinks />}
                changeColorOnScroll={{
                    height: 80,
                    color: "white",
                }}
                {...rest}
            />
            <div className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}>
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={10} lg={10}>
                            <Card className={classes[cardAnimaton]} style={{ background: '#EEEEEE' }}>
                                <CardHeader color="primary" className={classes.cardHeader}>
                                    <h1>Quản lý bài viết</h1>
                                </CardHeader>
                                <CardBody>
                                    <GridContainer justify="center">
                                        <GridItem xs={12} sm={12} md={12} lg={12}>
                                            <Button onClick={() => prepareAddPost()}>
                                                <PlusIcon /> Thêm bài viết
                                            </Button>
                                            <CustomInput
                                                inputRootCustomClasses={classes.inputRootCustomClasses}
                                                formControlProps={{
                                                    className: classes.formControl
                                                }}
                                                inputProps={{
                                                    placeholder: "Tìm kiếm bài viết",
                                                    inputProps: {
                                                        "aria-label": "Tìm kiếm bài viết",
                                                        className: classes.searchInput,
                                                        value: keyword,
                                                        onChange: onChangeKeyword
                                                    }
                                                }}
                                            />
                                            <Button justIcon round color="white" onClick={() => { dispatch(postAction.searchPost(keyword)) }}>
                                                <Search className={classes.searchIcon} />
                                            </Button>
                                        </GridItem>
                                        <GridItem xs={12} sm={12} md={12} lg={12} className={classes.boxTable}>
                                            <div className="table-responsive">
                                                <table className="table table-shopping">
                                                    <thead>
                                                        <tr>
                                                            <th className="text-left col-md-1">
                                                                <div className={classes.tableHeader}>
                                                                    #
                                                                </div>
                                                            </th>
                                                            <th className="text-left col-md-2">
                                                                <div className={classes.tableHeader}>
                                                                    Thumb
                                                                </div>
                                                            </th>
                                                            <th className="text-left col-md-4">
                                                                <div className={classes.tableHeader}>
                                                                    Tên bài viết
                                                                </div>
                                                            </th>
                                                            <th className="text-left col-md-2">
                                                                <div className={classes.tableHeader}>
                                                                    Chuyên mục
                                                                </div>
                                                            </th>
                                                            <th className="text-center col-md-2">
                                                                <div className={classes.tableHeader}>
                                                                    Ngày đăng
                                                                </div>
                                                            </th>
                                                            <th className="text-center col-md-2">
                                                                <div className={classes.tableHeader}>
                                                                    View
                                                                </div>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {posts.map((post, index) => (
                                                            <tr key={index}>
                                                                <td className="text-left col-md-1">
                                                                    <div className={classes.tableRow}>
                                                                        {index + 1}
                                                                    </div>
                                                                </td>
                                                                <td className="text-left col-md-2">
                                                                    <div className={classes.boxImage}>
                                                                        <img
                                                                            className={classes.thumb}
                                                                            src={post.thumb}
                                                                        />
                                                                    </div>
                                                                </td>
                                                                <td className="text-left col-md-4">
                                                                    <div className={classes.tableRow}>
                                                                        {post.title}
                                                                    </div>
                                                                </td>
                                                                <td className="text-left col-md-2">
                                                                    <div className={classes.tableRow}>
                                                                        {post.category}
                                                                    </div>
                                                                </td>
                                                                <td className="text-center col-md-2">
                                                                    <div className={classes.tableRow}>
                                                                        {baseHelper.formatStrToDate(post.created_at)}
                                                                    </div>
                                                                </td>
                                                                <td className="text-center col-md-2">
                                                                    <div className={classes.tableRow}>
                                                                        {post.views}
                                                                    </div>
                                                                </td>
                                                                <td className="td-actions text-right">
                                                                    <div className={classes.boxAction}>
                                                                        <button type="button" className="btn btn-warning" style={{ marginRight: '5px' }}
                                                                            onClick={() => viewPost(post)}>
                                                                            <Visibility style={{ verticalAlign: 'top' }} />
                                                                        </button>
                                                                        <button type="button" className="btn btn-info"
                                                                            onClick={() => prepareUpdatePost(post)}>
                                                                            <Edit style={{ verticalAlign: 'top' }} />
                                                                        </button>
                                                                        <button type="button" className="btn btn-danger" style={{ marginLeft: '5px' }}
                                                                            onClick={() => prepareDeletePost(post)}>
                                                                            <Close style={{ verticalAlign: 'top' }} />
                                                                        </button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </GridItem>
                                    </GridContainer>
                                </CardBody>
                            </Card>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
            <Dialog
                classes={{
                    root: classes.center,
                    paper: classes.modal
                }}
                open={openModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => setOpenModal(false)}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"
                maxWidth="xl"
                disableEnforceFocus={true}
            >
                <DialogTitle
                    id="classic-modal-slide-title"
                    disableTypography
                    className={classes.modalHeader}
                >
                    <IconButton
                        className={classes.modalCloseButton}
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={() => setOpenModal(false)}
                    >
                        <Close className={classes.modalClose} />
                    </IconButton>
                    <h4 className={classes.modalTitle}>Thêm bài viết</h4>
                </DialogTitle>
                <DialogContent
                    id="classic-modal-slide-description"
                    className={classes.modalBody}
                >
                    <GridContainer className={classes.formBox}>
                        <GridItem xs={12} sm={4} md={4} lg={4}>
                            <CustomInput
                                id="title"
                                inputProps={{
                                    placeholder: "Tiêu đề",
                                    value: post.title,
                                    onChange: onChangeTitle
                                }}
                                formControlProps={{
                                    fullWidth: true,
                                    style: { paddingTop: '0px' }
                                }}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={4} md={4} lg={4}>
                            <CustomDropdown
                                onClick={e => { onSelectCategory(e); }}
                                buttonText={`${post.category || `Chuyên mục`}`}
                                buttonProps={{
                                    className: classes.button,
                                    color: "transparent",
                                }}
                                dropdownList={['Tin tức', 'Video bàn thắng']}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={4} md={4} lg={4}>
                            <CustomInput
                                id="thumb"
                                inputProps={{
                                    placeholder: "Đường dẫn ảnh thumb",
                                    value: post.thumb,
                                    onChange: onChangeThumb
                                }}
                                formControlProps={{
                                    fullWidth: true,
                                    style: { paddingTop: '0px' }
                                }}
                            />
                        </GridItem>
                    </GridContainer>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12} lg={12}>
                            <Editor
                                key={editorKey}
                                apiKey="sx1fr3egn5szn3xio8lvelgjlp0hti6p6sn2wyrecje47j84"
                                value={post.content}
                                init={{
                                    height: 500,
                                    width: 700,
                                    menubar: false,
                                    plugins: [
                                        'advlist autolink lists link image charmap print preview anchor',
                                        'searchreplace visualblocks code fullscreen',
                                        'insertdatetime media table paste code help wordcount',
                                        'image link save table template media',
                                    ],
                                    toolbar:
                                        'undo redo | formatselect | bold italic underline backcolor |alignleft aligncenter alignright alignjustify | image |link openlink unlink| cancel save |table tableinsertdialog tablecellprops tabledelete tabledeletecol tabledeleterow  tableinsertcolafter tableinsertcolbefore tableinsertrowafter tableinsertrowbefore tablemergecells tableprops   tablerowprops  tablesplitcells | template bullist numlist outdent indent | removeformat |code| help | media',
                                    media_url_resolver: function (data, resolve) {
                                        let url = data.url;

                                        if (data.url.includes('youtube.com/watch')) {
                                            var res = data.url.split("=");
                                            url = "https://www.youtube.com/embed/" + res[1];
                                        }

                                        var embedHtml = '<p class="ytb"><iframe src="' + url +
                                            '" width="100%" height="100%" frameborder="0" allowfullscreen></iframe></p>';
                                        resolve({ html: embedHtml });
                                        setMediaUrl(data.url);
                                    }
                                }}
                                onEditorChange={(data) => handleEditorChange(data)}
                            />
                        </GridItem>
                    </GridContainer>
                </DialogContent>
                <DialogActions className={classes.modalFooter}>
                    <Button
                        onClick={() => savePost()}
                        color="success"
                        simple>Lưu lại</Button>
                    <Button
                        onClick={() => setOpenModal(false)}
                        color="danger"
                        simple>Đóng</Button>
                </DialogActions>
            </Dialog>
            <Dialog
                classes={{
                    root: classes.center,
                    paper: classes.modal
                }}
                open={openModalConfirm}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => setOpenModalConfirm(false)}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"
                maxWidth="xl"
                disableEnforceFocus={true}
            >
                <DialogTitle
                    id="classic-modal-slide-title"
                    disableTypography
                    className={classes.modalHeader}
                >
                    <IconButton
                        className={classes.modalCloseButton}
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={() => setOpenModalConfirm(false)}
                    >
                        <Close className={classes.modalClose} />
                    </IconButton>
                    <h4 className={classes.modalTitle}>Xác nhận</h4>
                </DialogTitle>
                <DialogContent
                    id="classic-modal-slide-description"
                    className={classes.modalBody}>
                    Bạn có chắc chắn xóa bài viết này?
                </DialogContent>
                <DialogActions className={classes.modalFooter}>
                    <Button
                        onClick={() => dispatch(postAction.deletePost(post._id))}
                        color="success"
                        simple>Xác nhận</Button>
                    <Button
                        onClick={() => setOpenModalConfirm(false)}
                        color="danger"
                        simple>Đóng</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
