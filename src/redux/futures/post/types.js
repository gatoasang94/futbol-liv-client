export const SAVE_POST = 'SAVE_POST';
export const DELETE_POST = 'DELETE_POST';
export const GET_POSTS = 'GET_POSTS';
export const SEARCH_POST = 'SEARCH_POST';
export const SHOW_POST = 'SHOW_POST';
export const LIST = 'LIST';
