import { REQUEST_METHOD } from '../../middlewares/services';

import * as types from './types';
import { endpoint } from '../../../config/app';

const savePost = (post) => ({
    type: types.SAVE_POST,
    meta: {
        endpoint: endpoint.POST_SAVE,
        method: REQUEST_METHOD.POST,
        body: { post },
        hasAuth: true,
    },
});

const getPosts = (category) => ({
    type: types.GET_POSTS,
    meta: {
        endpoint: endpoint.POST_GET_ALL,
        method: REQUEST_METHOD.POST,
        body: { category },
        hasAuth: true,
    },
});

const deletePost = (id) => ({
    type: types.DELETE_POST,
    meta: {
        endpoint: endpoint.POST_DELETE,
        method: REQUEST_METHOD.POST,
        body: { id },
        hasAuth: true,
    },
});

const searchPost = (keyword) => ({
    type: types.SEARCH_POST,
    meta: {
        endpoint: endpoint.POST_SEARCH,
        method: REQUEST_METHOD.POST,
        body: { keyword },
        hasAuth: true,
    },
});

const show = (code) => ({
    type: types.SHOW_POST,
    meta: {
        endpoint: endpoint.POST_SHOW,
        method: REQUEST_METHOD.POST,
        body: { code },
        hasAuth: true,
    },
});

const list = (data) => ({
    type: types.LIST,
    meta: {
        endpoint: endpoint.POST_LIST,
        method: REQUEST_METHOD.POST,
        body: {
            ...data
        },
    },
});

export default {
    savePost,
    getPosts,
    deletePost,
    searchPost,
    show,
    list
};
