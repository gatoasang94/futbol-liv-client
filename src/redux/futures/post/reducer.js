import * as types from './types';
import * as commonTypes from '../common/types';

export default (state = {}, action) => {
    switch (action.type) {
        case commonTypes.ASYNC_START: {
            if (!types[action.waitting]) {
                return state;
            }
            return {
                ...state,
                loading: true,
                waitting: action.waitting,
                action: commonTypes.ASYNC_START,
            };
        }

        case commonTypes.ASYNC_END: {
            if (!types[action.done]) {
                return state;
            }

            return {
                ...state,
                waitting: null,
                loading: false,
                action: commonTypes.ASYNC_END,
            };
        }

        case types.SAVE_POST: {
            return {
                ...state,
                action: types.SAVE_POST,
                post: action.payload ? action.payload.post : null,
                status: action.status,
                message: action.message,
            };
        }

        case types.GET_POSTS: {
            return {
                ...state,
                action: types.GET_POSTS,
                posts: action.payload.posts,
                status: action.status,
                message: action.message,
            };
        }

        case types.DELETE_POST: {
            return {
                ...state,
                action: types.DELETE_POST,
                status: action.status,
                message: action.message,
            };
        }

        case types.SEARCH_POST: {
            return {
                ...state,
                action: types.SEARCH_POST,
                posts: action.payload.posts,
                status: action.status,
                message: action.message,
            };
        }

        case types.SHOW_POST: {
            return {
                ...state,
                action: types.SHOW_POST,
                post: action.payload.post,
                status: action.status,
                message: action.message,
            };
        }

        case types.LIST: {
            return {
                ...state,
                action: types.LIST,
                posts: action.payload.posts,
                total: action.payload.total,
                page: action.payload.page,
                status: action.status,
                message: action.message,
            };
        }

        default:
            return state;
    }
};
