import { REQUEST_METHOD } from '../../middlewares/services';

import * as types from './types';
import { endpoint } from '../../../config/app';

const getPlayers = (league) => ({
    type: types.GET_PLAYERS,
    meta: {
        endpoint: endpoint.SCHEDULE_GET_PLAYERS,
        method: REQUEST_METHOD.POST,
        body: { league: league },
        hasAuth: true,
    },
});

export default {
    getPlayers,
};
