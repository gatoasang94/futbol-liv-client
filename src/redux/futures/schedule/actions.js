import { REQUEST_METHOD } from "../../middlewares/services";

import * as types from "./types";
import { endpoint } from "../../../config/app";

const getStreams = (offset) => ({
    type: types.GET_STREAMS,
    meta: {
        endpoint: endpoint.SCHEDULE_GET_STREAMS,
        method: REQUEST_METHOD.POST,
        body: {
            offset
        },
        hasAuth: true,
    },
});

const getAllStreams = () => ({
    type: types.GET_ALL_STREAMS,
    meta: {
        endpoint: endpoint.SCHEDULE_GET_ALL_STREAMS,
        method: REQUEST_METHOD.POST,
        body: {
        },
        hasAuth: true,
    },
});

const getResulStreams = () => ({
    type: types.GET_RESULT_STREAMS,
    meta: {
        endpoint: endpoint.SCHEDULE_GET_RESULT_STREAMS,
        method: REQUEST_METHOD.POST,
        body: {
        },
        hasAuth: true,
    },
});

const showStream = (code) => ({
    type: types.SHOW_STREAM,
    meta: {
        endpoint: endpoint.SCHEDULE_SHOW_STREAM,
        method: REQUEST_METHOD.POST,
        body: { code },
        hasAuth: true,
    },
});

const showSettingStream = (id) => ({
    type: types.SHOW_SETTING_STREAM,
    meta: {
        endpoint: endpoint.SCHEDULE_SHOW_SETTING_STREAM,
        method: REQUEST_METHOD.POST,
        body: { id },
        hasAuth: true,
    },
});

const addStreamLink = (streamID, linkInfo) => ({
    type: types.ADD_STREAM_LINK,
    meta: {
        endpoint: endpoint.SCHEDULE_ADD_STREAM_LINK,
        method: REQUEST_METHOD.POST,
        body: { stream_id: streamID, ...linkInfo },
        hasAuth: true,
    },
});

const deleteStreamLink = (streamID, url) => ({
    type: types.DELETE_STREAM_LINK,
    meta: {
        endpoint: endpoint.SCHEDULE_DELETE_STREAM_LINK,
        method: REQUEST_METHOD.POST,
        body: { stream_id: streamID, url },
        hasAuth: true,
    },
});

const getLiveStream = (type) => ({
    type: types.GET_LIVE_STREAM,
    meta: {
        endpoint: endpoint.SCHEDULE_GET_LIVE_STREAM,
        method: REQUEST_METHOD.POST,
        body: { type },
        hasAuth: true,
    },
});

const updateOrder = (streamID, order) => ({
    type: types.UPDATE_ORDER,
    meta: {
        endpoint: endpoint.SCHEDULE_UPDATE_ORDER,
        method: REQUEST_METHOD.POST,
        body: { stream_id: streamID, order },
        hasAuth: true,
    },
});

const updateThumbURL = (streamID, url) => ({
    type: types.UPDATE_THUMB,
    meta: {
        endpoint: endpoint.SCHEDULE_UPDATE_THUMB,
        method: REQUEST_METHOD.POST,
        body: { stream_id: streamID, thumb_url: url },
        hasAuth: true,
    },
});

export default {
    getStreams,
    getAllStreams,
    getResulStreams,
    showStream,
    showSettingStream,
    addStreamLink,
    deleteStreamLink,
    getLiveStream,
    updateOrder,
    updateThumbURL
};
