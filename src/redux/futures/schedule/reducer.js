import * as types from './types';
import * as commonTypes from '../common/types';

export default (state = {}, action) => {
    switch (action.type) {
        case commonTypes.ASYNC_START: {
            if (!types[action.waitting]) {
                return state;
            }
            return {
                ...state,
                loading: true,
                waitting: action.waitting,
                action: commonTypes.ASYNC_START,
            };
        }

        case commonTypes.ASYNC_END: {
            if (!types[action.done]) {
                return state;
            }

            return {
                ...state,
                waitting: null,
                loading: false,
                action: commonTypes.ASYNC_END,
            };
        }

        case types.GET_STREAMS: {
            return {
                ...state,
                action: types.GET_STREAMS,
                streams: action.payload.streams,
                status: action.status,
                message: action.message,
            };
        }

        case types.GET_LIVE_STREAM: {
            return {
                ...state,
                action: types.GET_LIVE_STREAM,
                streams: action.payload ? action.payload.streams : [],
                hotStreams: action.payload ? action.payload.hot_streams : [],
                status: action.status,
                message: action.message,
            };
        }


        case types.GET_ALL_STREAMS: {
            return {
                ...state,
                action: types.GET_ALL_STREAMS,
                allStreams: action.payload.streams,
                status: action.status,
                message: action.message,
            };
        }

        case types.GET_RESULT_STREAMS: {
            return {
                ...state,
                action: types.GET_RESULT_STREAMS,
                allStreams: action.payload.streams,
                status: action.status,
                message: action.message,
            };
        }

        case types.SHOW_STREAM: {
            return {
                ...state,
                action: types.SHOW_STREAM,
                stream: action.payload.stream,
                status: action.status,
                message: action.message,
            };
        }

        case types.SHOW_SETTING_STREAM: {
            return {
                ...state,
                action: types.SHOW_SETTING_STREAM,
                stream: action.payload.stream,
                status: action.status,
                message: action.message,
            };
        }

        case types.ADD_STREAM_LINK: {
            return {
                ...state,
                action: types.ADD_STREAM_LINK,
                stream: action.payload.stream,
                status: action.status,
                message: action.message,
            };
        }

        case types.DELETE_STREAM_LINK: {
            return {
                ...state,
                action: types.DELETE_STREAM_LINK,
                stream: action.payload.stream,
                status: action.status,
                message: action.message,
            };
        }

        case types.UPDATE_ORDER: {
            return {
                ...state,
                action: types.UPDATE_ORDER,
                status: action.status,
                message: action.message,
            };
        }

        case types.UPDATE_THUMB: {
            return {
                ...state,
                action: types.UPDATE_THUMB,
                status: action.status,
                message: action.message,
            };
        }

        default:
            return state;
    }
};
