import { combineReducers } from 'redux';
import scheduleReducer from '../futures/schedule/reducer';
import teamReducer from '../futures/team/reducer';
import userReducer from '../futures/user/reducer';
import playerReducer from '../futures/player/reducer';
import postReducer from '../futures/post/reducer';

export default (history) => combineReducers({
    schedule: scheduleReducer,
    team: teamReducer,
    user: userReducer,
    player: playerReducer,
    post: postReducer,
});