import * as types from './types';

const redirect = (path = '/') => ({
    type: types.REDIRECT,
    redirectTo: path,
});

const asyncStart = () => ({
    type: types.ASYNC_START,
});

export default {
    redirect,
    asyncStart,
}