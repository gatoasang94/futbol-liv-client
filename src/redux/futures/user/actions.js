import { REQUEST_METHOD } from '../../middlewares/services';

import * as types from './types';
import { endpoint } from '../../../config/app';

const loginFB = (token) => ({
    type: types.LOGIN_FB,
    meta: {
        endpoint: endpoint.USER_LOGIN_FB,
        method: REQUEST_METHOD.POST,
        body: { token: token },
        hasAuth: true,
    },
});

export default {
    loginFB,
};
