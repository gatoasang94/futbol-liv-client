import { REQUEST_METHOD } from '../../middlewares/services';

import * as types from './types';
import { endpoint } from '../../../config/app';

const getTeams = (league) => ({
  type: types.GET_TEAMS,
  meta: {
    endpoint: endpoint.SCHEDULE_GET_TEAMS,
    method: REQUEST_METHOD.POST,
    body: { league: league },
    hasAuth: true,
  },
});

export default {
  getTeams,
};
