import axios from 'axios';
import * as commonTypes from '../futures/common/types';

import appConfig, { status } from '../../config/app';

import { has, omit } from 'lodash';
import baseHelper from '../../helpers/BaseHelper';

export const REQUEST_METHOD = {
    GET: 'get',
    POST: 'post',
}

const serviceMiddleware = store => next => action => {
    next({ type: commonTypes.ASYNC_START, waitting: action.type });
    if (action.meta) {
        const { endpoint, body, method, hasAuth } = action.meta;
        let role;
        try {
            role = JSON.parse(localStorage.getItem(`FBL/${appConfig.LOCAL_FORAGE.USER_INFO}`)).role;
        } catch (e) {
        }

        body.role = role || 'MEMBER';

        const req = {
            method: method,
            url: endpoint,
            headers: requestHeaders(hasAuth, body),
            data: body,
        };

        axios(req).then(function (response) {
            if (action.type != commonTypes.ASYNC_END && action.type != commonTypes.ASYNC_START) {
            }
            const { data } = response;
            if (has(response.headers, 'store-id')) {

                action.new_store_id = response.headers['store-id'];
            }

            switch (data.code) {
                case status.OK:
                case status.CREATED:
                    action.status = true;
                    action.payload = data.data;
                    action.message = data.message;

                    // Handle cache data
                    cacheData(action);
                    break;
                case status.UNAUTHORIZED:
                    baseHelper.removeCookie(appConfig.COOKIE.SSID);
                    baseHelper.removeCookie(appConfig.COOKIE.STORE_ID);
                    localStorage.clear();
                    next({});
                    return;
                case status.FORBIDDEN:
                    action.status = false;
                    action.message = data.message;
                    break;
                default:
                    action.status = false;
                    action.message = data.message;
                    break;
            }

            next(action);
            next({ type: commonTypes.ASYNC_END, done: action.type });

        }).catch(function (error) {
            action.status = false;
            action.message = error.message;

            next({ type: commonTypes.ASYNC_END, done: action.type });
        });

        return;
    }

    next(action);
    next({ type: commonTypes.ASYNC_END, done: action.type });
}


const localStorageMiddleware = store => next => action => {
    next(action);
};

function requestHeaders(hasAuth = true, body = {}) {
    const timestamp = new Date().getTime();

    const headers = {
        'content-type': 'application/json',
        'timestamp': timestamp
    }
    if (!hasAuth) {
        return headers;
    }

    headers['x-hub-signature'] = baseHelper.getSignatureBySecretKey(baseHelper.getSortedStringFromObject(body), timestamp, appConfig.AUTH.SECRET_KEY || '');

    return headers;
}

async function cacheData(action) {
}


export { serviceMiddleware, localStorageMiddleware }
