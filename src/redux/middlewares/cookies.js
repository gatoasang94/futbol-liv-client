import appConfig, { status } from '../../config/app';
import baseHelper from '../../helpers/BaseHelper';
import { has } from 'lodash';

const cookiesMiddleware = store => next => action => {
    if (has(action, 'new_store_id')) {
        const oldStoreID = baseHelper.getCookie(appConfig.COOKIE.STORE_ID);
        if (oldStoreID != action.new_store_id) {
            baseHelper.setCookie(appConfig.COOKIE.STORE_ID, action.new_store_id);
            window.location.reload();
        }
    }

    next(action);
};

export default cookiesMiddleware;
