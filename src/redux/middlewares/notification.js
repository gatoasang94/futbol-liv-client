const notificationMiddleware = store => next => action => {
    next(action);
};

export default notificationMiddleware;
