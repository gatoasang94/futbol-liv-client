import localForage from 'localforage';

const appLocalStorage = localForage.createInstance({
    driver: localForage.LOCALSTORAGE,
    name: 'FBL',
});

const appIndexedDB = localForage.createInstance({
    driver: localForage.INDEXEDDB,
    name: 'FBL',
});

export {
    appLocalStorage,
    appIndexedDB,
};