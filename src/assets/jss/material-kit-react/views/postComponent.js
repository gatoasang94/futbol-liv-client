import { container } from "assets/jss/material-kit-react.js";
import headerLinksStyle from "assets/jss/material-kit-react/components/headerLinksStyle.js";
import modalStyle from "assets/jss/material-kit-react/modalStyle.js";

const managePostStyle = theme => ({
    container: {
        ...container,
        zIndex: "2",
        position: "relative",
        paddingTop: "20vh",
        color: "#FFFFFF",
        paddingBottom: "200px"
    },
    cardHidden: {
        opacity: "0",
        transform: "translate3d(0, -60px, 0)"
    },
    pageHeader: {
        minHeight: "100vh",
        height: "auto",
        display: "inherit",
        position: "relative",
        margin: "0",
        padding: "0",
        border: "0",
        alignItems: "center",
        "&:before": {
            background: "rgba(0, 0, 0, 0.5)"
        },
        "&:before,&:after": {
            position: "absolute",
            zIndex: "1",
            width: "100%",
            height: "100%",
            display: "block",
            left: "0",
            top: "0",
            content: '""'
        },
        "& footer li a,& footer li a:hover,& footer li a:active": {
            color: "#FFFFFF"
        },
        "& footer": {
            position: "absolute",
            bottom: "0",
            width: "100%"
        }
    },
    form: {
        margin: "0"
    },
    cardHeader: {
        width: "auto",
        textAlign: "center",
        marginLeft: "20px",
        marginRight: "20px",
        marginTop: "-40px",
        padding: "20px 0",
        marginBottom: "15px",
    },
    socialIcons: {
        maxWidth: "24px",
        marginTop: "0",
        width: "100%",
        transform: "none",
        left: "0",
        top: "0",
        height: "100%",
        lineHeight: "41px",
        fontSize: "20px"
    },
    divider: {
        marginTop: "30px",
        marginBottom: "0px",
        textAlign: "center"
    },
    cardFooter: {
        paddingTop: "0rem",
        border: "0",
        borderRadius: "6px",
        justifyContent: "center !important"
    },
    socialLine: {
        marginTop: "1rem",
        textAlign: "center",
        padding: "0"
    },
    inputIconsColor: {
        color: "#495057"
    },
    teamName: {
        fontWeight: "bold"
    },
    headerLink: {
        color: 'blue',
        fontSize: '18px',
        paddingTop: '30px'
    },
    searchIcon: {
        width: "20px",
        height: "20px",
        color: "inherit"
    },
    formControl: {
        margin: "0 !important",
        paddingTop: "0"
    },
    inputRootCustomClasses: {
        marginLeft: '50px'
    },
    searchIcon: {
        width: "20px",
        height: "20px",
        color: "inherit"
    },
    searchInput: {
        color: '#000'
    },
    ...headerLinksStyle(theme),
    ...modalStyle,
    title: {
        color: '#3C4858',
        marginTop: '30px',
        minHeight: '32px',
        fontFamily: '"Roboto Slab", "Times New Roman", serif',
        fontWeight: '700',
        marginBottom: '25px',
        textDecoration: 'none',
        fontSize: '20px',
        lineHeight: '1.4em'
    },
    content: {
        color: '#555',
        fontSize: '16px',
        "& > p.ytb:first-child": {
            width: "100%",
            "@media (min-width: 200px)": {
                height: "280px"
            },
            "@media (min-width: 576px)": {
                height: "300px"
            },
            "@media (min-width: 768px)": {
                height: "350px"
            },
            "@media (min-width: 992px)": {
                height: "400px"
            },
            "@media (min-width: 1200px)": {
                height: "450px"
            }
        },
        "& > p > iframe": {
            width: "100%",
            "@media (min-width: 200px)": {
                maxHeight: "280px"
            },
            "@media (min-width: 576px)": {
                maxHeight: "300px"
            },
            "@media (min-width: 768px)": {
                maxHeight: "350px"
            },
            "@media (min-width: 992px)": {
                maxHeight: "400px"
            },
            "@media (min-width: 1200px)": {
                maxHeight: "450px"
            }
        }
    },
    titleBox: {
        textAlign: 'center'
    },
});

export default managePostStyle;
