import { container } from "assets/jss/material-kit-react.js";
import modalStyle from "assets/jss/material-kit-react/modalStyle.js";

const tabsStyle = {
    section: {
        background: "#EEEEEE",
        padding: "20px 0",
    },
    container,
    textCenter: {
        textAlign: "center"
    },
    teamLogoStream: {
        width: "auto",
        height: "auto",
        "@media (max-width: 576px)": {
            maxWidth: "60px",
            maxHeight: "60px",
        },
        "@media (min-width: 576px)": {
            maxWidth: "60px",
            maxHeight: "60px"
        },
        "@media (min-width: 768px)": {
            maxWidth: "40px",
            maxHeight: "40px"
        },
        "@media (min-width: 960px)": {
            maxWidth: "30px",
            maxHeight: "30px"
        },
        "@media (min-width: 1200px)": {
            maxWidth: "45px",
            maxHeight: "45px"
        },
        "@media (min-width: 1400px)": {
            maxWidth: "50px",
            maxHeight: "50px"
        }
    },
    leagueLogo: {
        maxWidth: "40px",
        maxHeight: "40px",
        width: "auto",
        height: "auto"
    },
    streamTableHeader: {
        fontSize: "16px"
    },
    modalIns: {
        fontSize: "18px"
    },
    ...modalStyle,
    headerSetupLink: {
        fontWeight: 'bold',
        fontSize: '16px',
        color: 'blue',
        marginTop: '30px'
    },
    notFoundText: {
        fontSize: '16px',
        marginTop: '10px'
    },
    buttonAddLink: {
        marginTop: '27px'
    },
    buttonLinkType: {
        marginTop: '27px'
    },
    headerTableLink: {
        fontWeight: 'bold'
    },
    leagueName: {
        color: 'blue',
        fontSize: '13px',
        fontWeight: 'bold',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        "@media (max-width: 576px)": {
            fontSize: "11px",
        },
        textTransform: 'uppercase'
    },
    streamTime: {
        "& > p": {
            fontSize: '16px',
            margin: '0',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            "@media (max-width: 576px)": {
                fontSize: "13px",
            }
        },
        fontSize: '18px',
        color: 'blue',
        justifyContent: 'center',
        display: 'inline-flex',
    },
    scoreBoard: {
        "& > p": {
            // overflowWrap: 'normal',
            margin: '0',
            color: '#000',
            fontWeight: 500,
            fontSize: '14px',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
        },
    },
    score: {
        padding: '0px',
        "& > p": {
            margin: '0',
            fontSize: '18px',
            "@media (max-width: 576px)": {
                fontSize: "16px",
            }
        },
    },
    infoBox: {
        textAlign: 'center'
    },
    logoBox: {
        alignSelf: 'center',
        justifyContent: 'center',
        display: 'flex'
    },
    buttonBox: {
        alignSelf: 'center',
        justifyContent: 'center',
        display: 'flex'
    },
    card: {
        borderRadius: '30px',
        marginTop: '10px',
        marginBottom: '10px',
    },
    loadingScene: {
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        width: '100%',
        height: '100%',
        position: 'fixed',
        backgroundPosition: 'center center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: '15%',
        zIndex: 3000,
        background: '#FFFFFF',
    },
    btnAction: {
        padding: '6px !important',
        marginTop: '5px',
        fontSize: '11px !important',
        marginRight: '5px',
        "@media (max-width: 576px)": {
            fontSize: "10px !important",
            marginTop: "5px"
        },
    },
    coloredShadow: {
        // some jss/css to make the cards look a bit better on Internet Explorer
        "@media all and (-ms-high-contrast: none), (-ms-high-contrast: active)": {
            display: "none !important"
        },
        transform: "scale(0.94)",
        top: "12px",
        filter: "blur(12px)",
        position: "absolute",
        width: "100%",
        height: "100%",
        backgroundSize: "cover",
        zIndex: "-1",
        transition: "opacity .45s",
        opacity: "0"
    },
    cardTitle: {
        "&, & a": {
            color: '#3C4858',
            textDecoration: "none",
            fontWeight: "500",
            marginTop: "30px",
            marginBottom: "25px",
            minHeight: "32px",
            fontFamily: `"Roboto Slab", "Times New Roman", serif`,
            marginTop: ".625rem",
            marginBottom: "0.75rem",
            minHeight: "auto",
            cursor: "pointer",
            fontSize: "14px"
        }
    },
    description: {
        color: '#999',
        '&:hover': {
            cursor: 'pointer'
        },
        fontFamily: `"Roboto Slab", "Times New Roman", serif`,
    },
    imgThumb: {
        width: '400px',
        height: 'auto'
    },
    imgBox: {
        '&:hover': {
            cursor: 'pointer'
        }
    },
    category: {
        color: 'green',
        fontSize: '13px'
    },
    filterContainer: {
        textAlign: 'right'
    },
    btnFilter: {
        padding: '6px 10px 4px 10px'
    },
    grid: {
        paddingRight: '10px',
        paddingLeft: '10px'
    },
    matchWrapper: {
        "@media (min-width: 768px)": {
            width: "60%"
        },
    },
    postWrapper: {
        "@media (min-width: 768px)": {
            width: "60%"
        },
        padding: '0px'
    }
};

export default tabsStyle;
