import { container } from "assets/jss/material-kit-react.js";

const componentsStyle = {
    container,
    brand: {
        color: "#FFFFFF",
        textAlign: "left",
        zIndex: "1000"
    },
    title: {
        fontSize: "4.2rem",
        fontWeight: "600",
        display: "inline-block",
        position: "relative",
        color: "#fff708",
        zIndex: "1000"
    },
    subtitle: {
        fontSize: "2.313rem",
        maxWidth: "500px",
        margin: "10px 0 0",
        color: "#FFFFFF",
        fontWeight: 'bold',
        zIndex: "1000"
    },
    main: {
        background: "#FFFFFF",
        position: "relative",
        zIndex: "3"
    },
    mainRaised: {
        // top: '-200px',
        borderRadius: "6px",
        boxShadow:
            "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
        background: '#EEE'
    },
    link: {
        textDecoration: "none"
    },
    textCenter: {
        textAlign: "center"
    },
    topMatchContainer: {
        // position: 'absolute',
        zIndex: '9999',
        width: '100%',
        textAlign: '-webkit-center',
        paddingTop: '10vh',
        paddingBottom: '40px',
        paddingRight: '15px',
        paddingLeft: '15px'
    },
    matchTeam: {
        display: 'flex'
    },
    matchItem: {
        width: '33.33%'
    },
    countdown: {
        "@media (min-width: 768px)": {
            width: "40%"
        },
        fontFamily: "Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji'",
        color: '#fff',
        textAlign: 'center'
    },
    leagueName: {
        fontSize: '24px',
        fontWeight: '700'
    },
    timeContainer: {
        display: 'inline-flex',
    },
    matchTime: {
        color: 'red',
        paddingRight: '10px',
        fontWeight: 'bold',
        fontSize: '20px'
    },
    matchDate: {
        fontSize: '20px',
        color: '#fff',
        fontWeight: '400'
    },
    teamName: {
        fontWeight: '700',
        fontSize: '18px',
        marginTop: '5px'
    },
    teamLogoStream: {
        height: '105px',
        width: 'auto'
    },
    btnAction: {
        padding: '14px !important',
        fontSize: '16px !important',
        borderRadius: '15px !important',
        "@media (max-width: 576px)": {
            fontSize: "14px !important",
        },
    },
    countdownBox: {
        display: 'flex',
        justifyContent: 'center'
    },
    countdownText: {
        fontSize: '30px',
        fontWeight: '700',
        width: '75px',
        height: '75px',
        backgroundColor: '#013142',
        borderRadius: '5px',
        margin: '0px 5px'
    },
    background: {
        width: '100%',
        height: '100%'
    }
};

export default componentsStyle;
