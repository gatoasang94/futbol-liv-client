import { container } from "assets/jss/material-kit-react.js";

const tabsStyle = {
  section: {
    background: "#EEEEEE",
    padding: "20px 0"
  },
  container,
  textCenter: {
    textAlign: "center"
  },
  logo: {
    width: "80px",
    height: "80px",
  },
  streamTableHeader: {
    fontSize: "16px"
  },
  rankingTableHeader: {
    fontSize: "16px"
  },
  rankingTableRow: {
    fontSize: "15px"
  }
};

export default tabsStyle;
