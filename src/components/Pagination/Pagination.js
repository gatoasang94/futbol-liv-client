import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import styles from "assets/jss/material-kit-pro-react/components/paginationStyle.js";

const useStyles = makeStyles(styles);

export default function Pagination(props) {
    const classes = useStyles();
    const { total, currentPage, color, onClick } = props;
    const pages = [];
    const totalPage = parseInt(total / 10) +(total % 10 == 0 ? 0 : 1);

    for (let i = -2; i <= 2; i++) {
        if (currentPage + i > 0 && currentPage + i <= totalPage) {
            const data = { text: currentPage + i };
            if (i == 0)
                data.active = true;
            pages.push(data);
        }
    }

    if (currentPage > 1)
        pages.unshift({ text: 'Trước' })
    if (currentPage < totalPage)
        pages.push({ text: 'Sau' })
        
    return (
        <ul className={classes.pagination}>
            {/* <div style={{ paddingTop: '10px', paddingLeft: '10px' }}>
                {total >= currentPage * 10 ? currentPage * 10 : total}/{total} kết quả
            </div> */}
            {pages.map((prop, key) => {
                const paginationLink = cx({
                    [classes.paginationLink]: true,
                    [classes[color]]: prop.active,
                    [classes.disabled]: prop.disabled,
                });
                return (
                    <li className={classes.paginationItem} key={key}>
                        <Button onClick={(e) => onClick(e, prop.text)} className={paginationLink}>
                            {prop.text}
                        </Button>
                    </li>
                );
            })}
        </ul>
    );
}

Pagination.defaultProps = {
    color: "primary",
};

Pagination.propTypes = {
    pages: PropTypes.arrayOf(
        PropTypes.shape({
            active: PropTypes.bool,
            disabled: PropTypes.bool,
            text: PropTypes.oneOfType([
                PropTypes.number,
                PropTypes.oneOf(["PREV", "NEXT", "..."]),
            ]).isRequired,
        })
    ).isRequired,
    color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
    onClick: PropTypes.func,
    total: PropTypes.number,
    currentPage: PropTypes.number,
};
