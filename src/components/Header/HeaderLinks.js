/* global FB*/
import React, { useEffect, useState } from 'react';
import FacebookIcon from "@material-ui/icons/Facebook";
// react components for routing our app without refresh

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

import { appLocalStorage } from '../../localforage';
import appConfig from '../../config/app';
import baseHelper from '../../helpers/BaseHelper';

// @material-ui/icons
import { Apps, ListAltOutlined, HomeOutlined, MenuBook, FormatListNumbered, OndemandVideo, ListAlt } from "@material-ui/icons";

// core components
import Button from "components/CustomButtons/Button.js";
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";
import { useHistory, useLocation } from "react-router-dom";

// redux
import { useDispatch, useSelector } from 'react-redux';

// action
import userAction from '../../redux/futures/user/actions';

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
    const history = useHistory();
    const location = useLocation();
    const classes = useStyles();
    const dispatch = useDispatch();
    const userReducer = useSelector((state) => state.user);

    const [user, setUser] = useState(null);
    const [isAdmin, setIsAdmin] = useState(false);
    const [newMenuItems, setNewMenuItems] = useState([
        "Tin tức",
        { divider: true },
        "Video bàn thắng",
        ,]
    );
    const [scheduleMenuItems, setScheduleMenuItems] = useState([
        "Lịch thi đấu",
        { divider: true },
        "Kết quả",]
    );

    React.useEffect(() => {
        appLocalStorage.getItem(appConfig.LOCAL_FORAGE.USER_INFO)
            .then(data => {
                try {
                    setUser(data);

                    if (data && data.role === 'ADMIN') {
                        // const items = newMenuItems;
                        // items.unshift({ divider: true });
                        // items.unshift('Quản lý bài viết');
                        // setNewMenuItems(items);
                        setIsAdmin(true);
                    }
                } catch (e) { }
            });

        window.fbAsyncInit = function () {
            window.FB.init({
                appId: '298406651498880',
                cookie: true,
                xfbml: true,
                version: 'v6.0'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.async = true;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }, []);

    useEffect(() => {
        if (!userReducer.user)
            return;

        setUser(userReducer.user);

        // set local storage
        appLocalStorage.setItem(appConfig.LOCAL_FORAGE.USER_INFO, userReducer.user).then((data) => {
            try {
                props.action();
            } catch (e) { }
        });
    }, [userReducer.user]);

    const loginFB = () => {
        FB.login(function (response) {
            if (response.status === 'connected') {
                dispatch(userAction.loginFB(response.authResponse.accessToken));
            }
        }.bind(this), {
            scope: 'email'
        });
    }

    const onClickAvatarItem = (key) => {
        // logout
        if (key === 'Đăng xuất') {
            setUser(null);
            appLocalStorage.setItem(appConfig.LOCAL_FORAGE.USER_INFO, null).then((data) => {
                try {
                    props.action();
                } catch (e) { }
            });;
        }
    }

    const onClickNewMenuItems = (key) => {
        if (key === 'Quản lý bài viết')
            history.push(`/posts`);
        else {
            history.push(`/post/category/` + baseHelper.getAliasName(key));
        }
    }

    const onClickScheduleMenuItems = (key) => {
        if (key === 'Lịch thi đấu')
            history.push(`/schedule`);
        else {
            history.push(`/result`);
        }
    }

    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem} selected={location.pathname === "/"}>
                <Button
                    color="transparent"
                    target="_blank"
                    className={classes.navLink}
                    onClick={e => { history.push(`/`) }}
                >
                    <HomeOutlined className={classes.icons} /> Trang chủ
                </Button>
            </ListItem>
            <ListItem className={classes.listItem} selected={location.pathname === "/schedule" || location.pathname === "/result"}>
                <CustomDropdown
                    onClick={e => { onClickScheduleMenuItems(e); }}
                    buttonText="Lịch thi đấu"
                    buttonIcon={ListAltOutlined}
                    buttonProps={{
                        className: classes.navLink,
                        color: "transparent",
                    }}
                    dropdownList={scheduleMenuItems}
                />
            </ListItem>
            <ListItem className={classes.listItem} selected={location.pathname === "/ranking"}>
                <Button
                    color="transparent"
                    target="_blank"
                    className={classes.navLink}
                    onClick={e => { history.push(`/ranking`) }}
                >
                    <FormatListNumbered className={classes.icons} /> Bảng xếp hạng
                </Button>
            </ListItem>
            <ListItem className={classes.listItem} selected={location.pathname === "/post/category/tin-tuc"}>
                <Button
                    color="transparent"
                    target="_blank"
                    className={classes.navLink}
                    onClick={e => { history.push(`/post/category/tin-tuc`) }}
                >
                    <MenuBook className={classes.icons} /> Tin tức
                </Button>
            </ListItem>
            <ListItem className={classes.listItem} selected={location.pathname === "/post/category/video-ban-thang"}>
                <Button
                    color="transparent"
                    target="_blank"
                    className={classes.navLink}
                    onClick={e => { history.push(`/post/category/video-ban-thang`) }}
                >
                    <OndemandVideo className={classes.icons} /> Video
                </Button>
            </ListItem>
            {
                isAdmin
                &&
                <ListItem className={classes.listItem} selected={location.pathname === "/posts"}>
                    <Button
                        color="transparent"
                        target="_blank"
                        className={classes.navLink}
                        onClick={e => { history.push(`/posts`) }}
                    >
                        <ListAlt className={classes.icons} /> Quản lý bài viết
                    </Button>
                </ListItem>
            }

            {/* <ListItem className={classes.listItem} selected={location.pathname === "/news"}>
                <CustomDropdown
                    onClick={e => { onClickNewMenuItems(e); }}
                    buttonText="Chuyên mục"
                    buttonIcon={MenuBook}
                    buttonProps={{
                        className: classes.navLink,
                        color: "transparent",
                    }}
                    dropdownList={newMenuItems}
                />
            </ListItem> */}
            <ListItem className={classes.listItem}>
                {
                    user
                        ?
                        <CustomDropdown
                            onClick={key => onClickAvatarItem(key)}
                            left
                            caret={false}
                            hoverColor="success"
                            buttonText={
                                <img
                                    src={user.avatar}
                                    className={classes.img}
                                    alt="profile"
                                />
                            }
                            buttonProps={{
                                className:
                                    classes.imageDropdownButton,
                                color: "transparent"
                            }}
                            dropdownList={[
                                `${user.name}`,
                                "Đăng xuất"
                            ]}
                        />
                        :
                        <Button
                            color="transparent"
                            target="_blank"
                            className={classes.navLink}
                            onClick={loginFB}
                            aria-label="FB"
                        >
                            <FacebookIcon className={classes.socialIcons} />
                        </Button>
                }
            </ListItem>
        </List>
    );
}
