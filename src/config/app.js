import config from './config';

export const status = {
    OK: 200,
    CREATED: 201,
    NO_STORE: 204,
    BAD_REQUEST: 400,
    FORBIDDEN: 403,
    UNAUTHORIZED: 401,
    METHOD_NOT_ALLOWED: 405,
    REQUEST_TIMEOUT: 408,
    MANY_REQUEST: 429,
    EXPIRED_SESSION: 503,
    ERROR: 500,
};

export const endpoint = {
    // schedule
    SCHEDULE_GET_STREAMS: `${config.API_V1_0}/schedule/get-streams`,
    SCHEDULE_GET_ALL_STREAMS: `${config.API_V1_0}/schedule/get-all-streams`,
    SCHEDULE_GET_LIVE_STREAM: `${config.API_V1_0}/schedule/get-live-stream`,
    SCHEDULE_UPDATE_ORDER: `${config.API_V1_0}/schedule/update-order`,
    SCHEDULE_UPDATE_THUMB: `${config.API_V1_0}/schedule/update-thumb`,
    SCHEDULE_GET_RESULT_STREAMS: `${config.API_V1_0}/schedule/get-result-streams`,
    SCHEDULE_SHOW_STREAM: `${config.API_V1_0}/schedule/show-stream`,
    SCHEDULE_SHOW_SETTING_STREAM: `${config.API_V1_0}/schedule/show-setting-stream`,
    SCHEDULE_ADD_STREAM_LINK: `${config.API_V1_0}/schedule/add-stream-link`,
    SCHEDULE_DELETE_STREAM_LINK: `${config.API_V1_0}/schedule/delete-stream-link`,
    SCHEDULE_GET_TEAMS: `${config.API_V1_0}/team/get-teams`,
    SCHEDULE_GET_PLAYERS: `${config.API_V1_0}/player/get-players`,

    // user
    USER_LOGIN_FB: `${config.API_V1_0}/user/login-fb`,

    // post
    POST_SAVE: `${config.API_V1_0}/post/save`,
    POST_DELETE: `${config.API_V1_0}/post/delete`,
    POST_GET_ALL: `${config.API_V1_0}/post/get-all`,
    POST_SEARCH: `${config.API_V1_0}/post/search`,
    POST_SHOW: `${config.API_V1_0}/post/show`,
    POST_LIST: `${config.API_V1_0}/post/list`,
};

export default {
    MY_INFO: 'XY3J6326-kCAYN10v',
    ERROR: 'ERROR',
    TYPE_SAVE: {
        SAVE_DRAF: 'SAVE_DRAF',
        SAVE_POST: 'SAVE_POST',
    },
    COOKIE: {
        SSID: 'SSID',
        STORE_ID: 'STORE_ID',
        EXPIRE: 365,
    },
    LOCAL_FORAGE: {
        USER_INFO: 'USER_INFO',
    },
    VER: '1.0',
    DEFAULT_FORMAT_DATE_TIME: 'DD/MM/YYYY HH:mm',
    DEFAULT_FORMAT_DATE: 'dd/MM/yyyy',
    REPORT_OVERTIME_FORMAT_DATE: 'dd/MM',
    DEFAULT_DATE_PICKER_FORMAT_DATE: 'dd-MM-yyyy',
    DEFAUT_TIME: 'HH:mm',
    DB_DATE_TIME_FORMAT: 'dd-MM-yyyy HH:mm:ss',

    FORM_MODE: {
        CREATE: 'CREATE',
        EDIT: 'EDIT',
        VIEW: 'VIEW',
    },
    APP: {
        LP: 'lp',
        LF: 'lf',
    },
    STATUS: {
        ACTIVE: 1,
        INACTIVE: 0,
    },
    MODE: {
        TEST: 0,
        PRODUCTION: 1,
    },
    AUTH: {
        SECRET_KEY: '8y/B?E(H+MbQeThWmZq4t7w9z$C&F)J@',
    },
    TEAM: {
        LEAGUE: {
            PREMIER_LEAGUE: 'PREMIER_LEAGUE',
            LA_LIGA: 'LA_LIGA',
            SERIE_A: 'SERIE_A',
            BUNDESLIGA: 'BUNDESLIGA',
            LIGUE_1: 'LIGUE_1',
            V_LEAGUE: 'V_LEAGUE',
            C1: 'C1',
            C2: 'C2',
        }
    },
    POST: {
        CATEGORY: {
            LASTEST: {
                name: 'Lastest',
                alias: 'lastest',
            },
            VIDEO: {
                name: 'Video bàn thắng',
                alias: 'video-ban-thang',
            },
            NEWS: {
                name: 'Tin tức',
                alias: 'tin-tuc',
            },
            COMMENT: {
                name: 'Bình luận',
                alias: 'binh-luan',
            },
            TRANSFER: {
                name: 'Chuyển nhượng',
                alias: 'chuyen-nhuong'
            },
            FOOTBALL_HISTORY: {
                name: 'Lịch sử bóng đá',
                alias: 'lich-su-bong-da'
            }
        }
    }
};
