const HOST = 'viebongda.com';
// const HOST = 'localhost';

const config = {
    API_HOST: HOST,
    STREAM_HOST: 'stream.' + HOST,
    LIVE_HOST: 'live.' + HOST,
    API_V1_0: HOST === 'localhost' ? 'http://' + HOST + ':4560/1.0' : 'https://' + HOST + ':8443/1.0',
}

export default config;