import React, { Suspense, lazy } from 'react';
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux';
import { store } from './redux/store';

import "assets/scss/material-kit-react.scss?v=1.9.0";
require('./assets/scss/index.scss');

const HomePage = lazy(() => import('views/HomePage/HomePage.js'));
const Components = lazy(() => import('views/Components/Components.js'));
const LandingPage = lazy(() => import('views/LandingPage/LandingPage.js'));
const ProfilePage = lazy(() => import('views/ProfilePage/ProfilePage.js'));
const LoginPage = lazy(() => import('views/LoginPage/LoginPage.js'));
const WatchComponent = lazy(() => import('views/WatchComponent/WatchComponent.js'));
const RankingComponent = lazy(() => import('views/RankingComponent/RankingComponent'));
const ScheduleComponent = lazy(() => import('views/ScheduleComponent/ScheduleComponent'));
const ResultComponent = lazy(() => import('views/ResultComponent/ResultComponent'));
const ManagePostComponent = lazy(() => import('views/ManagePostComponent/ManagePostComponent'));
const PostCategoryComponent = lazy(() => import('views/ManagePostComponent/PostCategoryComponent'));
const PostComponent = lazy(() => import('views/ManagePostComponent/PostComponent'));

var hist = createBrowserHistory();

ReactDOM.render(
    <Provider store={store}>
        <Router history={hist}>
            <Suspense fallback={<div></div>}>
                <Switch>
                    <Route path="/landing-page" component={LandingPage} />
                    <Route path="/profile-page" component={ProfilePage} />
                    <Route path="/login-page" component={LoginPage} />
                    <Route path="/watch/:code" component={WatchComponent} />
                    <Route path="/preview/:code" component={WatchComponent} />
                    <Route path="/ranking" component={RankingComponent} />
                    <Route path="/schedule" component={ScheduleComponent} />
                    <Route path="/result" component={ResultComponent} />
                    <Route path="/component" component={Components} />
                    <Route path="/posts" component={ManagePostComponent} />
                    <Route path="/post/category/:category" component={PostCategoryComponent} />
                    <Route path="/post/:code" component={PostComponent} />
                    <Route path="/" component={HomePage} />
                    <Route path="/bot" component={HomePage} />
                </Switch>
            </Suspense>
        </Router>
    </Provider>,
    document.getElementById("root")
);
