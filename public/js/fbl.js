(function () {
    var _FBL = function () { };
    var a = 1;

    _FBL.prototype.toastr = function (type, title, message, timeout, position) {
        if (type == undefined) {
            type = 'error';
        }
        if (title == undefined) {
            title = '';
        }
        if (message == undefined) {
            message = '';
        }
        if (timeout == undefined) {
            timeout = 5000;
        }
        if (position == undefined) {
            position = 'top-right';
        }

        var toastContainer = document.getElementById("ladiui-toast-container");
        if (!toastContainer) {
            toastContainer = document.createElement("div");
            toastContainer.setAttribute("id", "ladiui-toast-container");
            toastContainer.setAttribute("class", "toast-" + position);
        }

        var toastType = document.createElement("div");
        toastType.setAttribute("class", "ladiui-toast toast-" + type);
        toastType.setAttribute("order", a++);

        var toastTitle = document.createElement("div");
        toastTitle.setAttribute("class", "ladiui-toast-title");
        title = document.createTextNode(title);
        toastTitle.appendChild(title);
        toastType.appendChild(toastTitle);

        var toastMessage = document.createElement("div");
        toastMessage.setAttribute("class", "ladiui-toast-message");
        toastMessage.innerHTML = message;
        toastType.appendChild(toastMessage);

        toastContainer.appendChild(toastType);

        document.body.appendChild(toastContainer);

        setTimeout(function () {
            toastType.remove();
        }.bind(toastType), timeout);
    };

    window.FBL = window.FBL || new _FBL();
})();